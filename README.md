**financtransaction**
=====================

 

É um projeto desenvolvido como desafio para criar um conjunto de operações que
simulem

o processo de uma transação financeira com cartão.

 

Github url: [Github
financtransaction](https://github.com/agmguerra/financtransaction)

 

**O projeto foi dividido em 2 subprojetos**:

1.  Contendo uma api rest para suportar as operações com Clientes, Cartões e
    Transações

2.  Uma aplicação cliente para ser utilizada para teste da api.

 

-   [Github
    stone-financ-client](https://github.com/agmguerra/financtransaction/tree/master/stone-financ-client)

-   [Github
    stone-financ-server](https://github.com/agmguerra/financtransaction/tree/master/stone-financ-server)

 

 

**Arquitetura Básica dos Projetos**
-----------------------------------

 

### **Domínio**

As classes do diagrama abaixo representam o modelo de domínio básico definido
para os projetos

 

![](images/diagrama_classe.png)

 

### **Request Padrão stone-financ-server**

 

![](images/request_padrao_server.svg)

-   No projeto stone-financ-server os request seguem o padrão descrito no
    diagrama acima.

 

### **Request Padrão stone-financ-client**

 

![](images/request_padrao_client.svg)

-   No projeto stone-financ-client os request seguem o padrão definido no
    diagrama acima. Todas as regras de negócio são implementadas através de uma
    api rest disponibilizada pelo projeto stone-financ-server

 

### **Controladores**

No projeto stone-financ-client o controller é implementado pela classe
MainController.java

 

![](images/MainController.png)

 

No projeto stone-financ-server os controladores são implementados através das
classes:

 

![](images/Resources.png)

 

 

### **Services**

Os serviços de negócio são acessados através de interfacces que são
implementadas seguindo basicamente o exemplo do diagrama abaixo:

 

![](images/Service.png)

 

No projeto cliente definiu-se uma classe abstrada BaseService.java que
implementa alguns métodos que são reaproveitados por todos os serviços.

 

### **Persistência**

 

A camada de persistência do projeto stone-financ-server foi implementada através
do Spring Data JPA. As interfaces anotadas com \@Repository definem as operações
necessárias.

 

![](images/repositorios.png)

 

**Segurança**
-------------

 

Nos dois projetos a autenticação acontece através da utilização do Spring
Security. A segurança é estabelecida através da utilização de token JWT.

 

No projeto stone-financ-server o fluxo de autenticação segue de forma resumida o
descrito abaixo:

 

![](images/seguranca_server.svg)

![](images/login.png)

 

Após a autenticação (login) para acessar a api rest (stone-financ-server) todos
os request devem enviar o token (jwt) através do parâmetro Authorization no
hearder.

 

No projeto stone-financ-client o fluxo de autenticação segue resumidamente o
descrito abaixo:

 

![](images/seguranca_client.svg)

Após a autenticação todos os requests devem enviar o cookie com nome
Authorization.

 

### **Observação:**

**Em uma aplicação real todos os requests tanto do browser para a aplicação
quanto da aplicação para a rest api devem ser realizadas através do protocolo
SSL.**

 

 

**Documentação da Api REST.**
-----------------------------

 

A documentação (resumida) da api rest implementada pelo projeto
stone-financ-server pode ser consultada após o projeto ser iniciado, através da
url: [Doc Rest API](http://localhost:8080/swagger-ui.html#/cartao-resource)

 

![](images/rest_api_doc.png)

 

 

**Executando os projetos**
--------------------------

 

Para executar os projetos podem ser utilizados os seguintes comandos, após
clonar ou baixar o zip do projeto através do GitHub.

1.  Ir para o diretório específico da aplicação client ou server

2.  Executar o comando: mvn spring-boot:run

3.  Os dois projetos deve estar iniciados.

4.  Para o projeto stone-financ-server a url é: http://localhost:8080

5.  Para o projeto stone-financ-client a url é: http://localhost:8081

 

 

**Carga inicial para testes**
-----------------------------

 

Ao executar os projetos é realizado uma carga inicial nas tabelas o que permite
que alguns testes sejam executados. Abaixo segue o conteúdo das tabelas no
momento em que os projetos são iniciados:

 

![](images/db_cliente.png)

 

![](images/db_cartao.png)

![](images/db-cartao_saldo.png)

![](images/db_transacao.png)

 

A carga é realizada através do arquivo import.sql:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
INSERT INTO CLIENTE(CLIENTE_ID, NOME, TP_DOCUMENTO, DOCUMENTO, USERNAME, PASSWORD) VALUES (1, 'Alexandre Guerra', 0, '98433683713', 'teste', '$2a$10$109cPYUgD3e5PhwaK1vftelpHV8IxfMQMIhkg15rKtn9Y6ixT4LlC');
INSERT INTO CLIENTE(CLIENTE_ID, NOME, TP_DOCUMENTO, DOCUMENTO, USERNAME, PASSWORD) VALUES (2, 'Manuel da Silva', 0, '24386858245', 'teste2', '$2a$10$109cPYUgD3e5PhwaK1vftelpHV8IxfMQMIhkg15rKtn9Y6ixT4LlC');

INSERT INTO CARTAO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, CLIENTE_ID, COD_SEG) VALUES (1, 'João da Silva', '1234567890123456', '12/18', 0, 0, 0, 1, '111');
INSERT INTO CARTAO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, CLIENTE_ID, COD_SEG) VALUES (2, 'Manuel da Silva', '1234567890123400', '12/19', 0, 0, 0, 2, '111');


INSERT INTO TRANSACAO ( TRANSACAO_ID, DATA_TRANSACAO, NUM_PARCELAS, TIPO_TRANSACAO, VALOR_TRANSACAO, CARTAO_ID ) VALUES (1, '2018-12-31', 0, 0, 100, 1)
INSERT INTO TRANSACAO ( TRANSACAO_ID, DATA_TRANSACAO, NUM_PARCELAS, TIPO_TRANSACAO, VALOR_TRANSACAO, CARTAO_ID ) VALUES (2, '2018-12-31', 0, 0, 100, 2)

INSERT INTO CARTAO_SALDO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, PASSWORD, SALDO, COD_SEG) VALUES (1, 'João da Silva', '1234567890123456', '12/18', 0, 0, 0, '$2a$10$yA6EckI6YuJL91FzM0kUU.2qKtqp32l5fMGSk4jw8mRiMlL9drOhq', 1000, '111');
INSERT INTO CARTAO_SALDO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, PASSWORD, SALDO, COD_SEG) VALUES (2, 'Manuel da Silva', '1234567890123400', '12/19', 0, 0, 0, '$2a$10$yA6EckI6YuJL91FzM0kUU.2qKtqp32l5fMGSk4jw8mRiMlL9drOhq', 1000, '111');
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

Para Realizar testes com outros clientes ou cartões será necessário incluir
antes do inicio do teste a linha referente a este cliente na tabela
CARTAO_SALDO. Já que está tabela assume a responsabilidade de conter as
informações do saldo por cartão.

 
