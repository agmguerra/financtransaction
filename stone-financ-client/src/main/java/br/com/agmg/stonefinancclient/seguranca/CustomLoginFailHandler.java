package br.com.agmg.stonefinancclient.seguranca;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 * 
 * Componente responsável por tratar os erros de login
 *
 */
@Component
public class CustomLoginFailHandler implements AuthenticationFailureHandler {

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
			
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
			
		redirectStrategy.sendRedirect(request, response, determineTargetUrl(exception));
		
	}		
	
	public RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}
 
	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}

	private String determineTargetUrl(AuthenticationException exception) {
		String ret = null;
		if (exception instanceof AuthenticationServiceException) {
			ret = "/login?internalError";
		} else if (exception instanceof BadCredentialsException) {
			ret = "/login?error";
		}
		
		return ret;
		
	}

	
}
