package br.com.agmg.stonefinancclient.seguranca;

import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.EXPIRATION_TIME;
import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.HEADER_STRING;
import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.SECRET;
import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.TOKEN_PREFIX;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 
 * Filtro responsável por inteceptar o request para login
 * e prover e caso de sucesso gerar o JWT token
 *
 */
public class JWTAuthenticationFilter extends  UsernamePasswordAuthenticationFilter {
	
	private AuthenticationManager authenticationManager;
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
	
	
	@Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
 
        	String username = req.getParameter("username");
        	String password = req.getParameter("password");
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password,
                            new ArrayList<>())
            );
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

		SecurityContextHolder.getContext().setAuthentication(auth);

    	
        String token = Jwts.builder()
                .setSubject(((String) auth.getPrincipal()))
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
                .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        Cookie jwtCookie = new Cookie("Authorization", URLEncoder.encode( TOKEN_PREFIX + token, "UTF-8" ));
        res.addCookie(jwtCookie);   
        chain.doFilter(req, res);
    }
    
    
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
    		AuthenticationException failed) throws IOException, ServletException {
    	
    	getFailureHandler().onAuthenticationFailure(request, response, failed);
    	
    }


	@Override
	protected AuthenticationFailureHandler getFailureHandler() {

		return new CustomLoginFailHandler();
	}
    
    

}
