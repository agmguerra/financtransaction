package br.com.agmg.stonefinancclient.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.Cookie;

import io.jsonwebtoken.Jwts;

/**
 * 
 * classe com métodos uteis para obter informações do token Jwt
 *
 */
public class JwtUtils {

	/**
	 * Retorna o username a partir do token jwt
	 * @param cookies array de cookies recebidos no request
	 * @param tokenPrefix prefixo do token
	 * @param tokenKey chave utilizada para criptografar o token
	 * @return Optional<String> contendo o username ou nulo
	 */
	public static Optional<String> getUsernameFromJwtCookie(Cookie[]cookies, String tokenPrefix, String tokenKey) {
		
		String user = null;
		Optional<String> jwtToken = getAuthorizationParam(cookies);
		if (jwtToken.isPresent()) {

			
	         user = Jwts.parser()
	                .setSigningKey(tokenKey.getBytes())
	                .parseClaimsJws(jwtToken.get().replace(tokenPrefix, ""))
	                .getBody()
	                .getSubject();
			 
		}
		return Optional.of(user);
	}
	
	/**
	 * Retorna o Jwt Authorization a partir dos cookies.
	 * @param cookies
	 * @return Optional<String> contendo o Authorization ou nulp
	 */
	public static Optional<String> getAuthorizationParam(Cookie[]cookies) {
		String param = null;
		if (cookies != null) {		
			param = Arrays.stream(cookies)
					.filter(cookie -> "Authorization".equals(cookie.getName()))
					.map(cookie -> {
						try {
							return URLDecoder.decode(cookie.getValue(), "UTF-8");
						} catch (UnsupportedEncodingException e1) {
							return null;
						}
					}).findAny().orElse(null);
			
			
			
//			for (Cookie cookie : cookies) {
//				if (cookie.getName().equals("Authorization")) {
//					try {
//						param = URLDecoder.decode(cookie.getValue(), "UTF-8");
//					} catch (UnsupportedEncodingException e) {
//						param = null;
//					}
//					break;
//				}
//			}
		}
		return Optional.of(param);
	}
}
