package br.com.agmg.stonefinancclient.seguranca;

import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.SECRET;
import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.TOKEN_PREFIX;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import br.com.agmg.stonefinancclient.exception.RestResponseErrorHandler;
import br.com.agmg.stonefinancclient.utils.RestUtil;
import io.jsonwebtoken.Jwts;

/**
 * 
 * AuthenticationProvider customizado para permitir usar o
 * serviço a api rest de serviços para autenticar
 *
 */

@Service
public class CustomRestAuthenticationProvider implements AuthenticationProvider {

	//TODO - Ver um modo de externalizar esse parâmetro
    private final static String URL_AUTENTICACAO = "http://localhost:8080/login";
	    
    
	/**
	 * @see AuthenticationProvider#authenticate(Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		String username = authentication.getName();
        String password = authentication.getCredentials().toString();
         
        
        return getAuthentication(username, password);
		
	}

	/**
	 * @see AuthenticationProvider#supports(Class)
	 */
	@Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
		
    private UsernamePasswordAuthenticationToken getAuthentication(String username, String password) {
    	
    	UsernamePasswordAuthenticationToken userPassToken = null;
    	
        Optional<HttpHeaders> headers = validateUserOnRestService(username, password);
        
        if (headers.isPresent()) {
	        String token = headers.get().getFirst(HttpHeaders.AUTHORIZATION);
	        if (token != null) {
	            // parse the token.
	            String user = Jwts.parser()
	                    .setSigningKey(SECRET.getBytes())
	                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
	                    .getBody()
	                    .getSubject();
	
	            if (user != null) {
	            	
	            	userPassToken = new UsernamePasswordAuthenticationToken(user, password, new ArrayList<>());
	            }
	        }
        }
        return userPassToken;
    }
	
	private Optional<HttpHeaders> validateUserOnRestService(String username, String password) {
		
		String json = getJsonUsernameAndPassword(username, password);
		
		try {
			RestTemplate rest = new RestTemplate();
			rest.setErrorHandler(getRestResponseErrorHandler());
			HttpEntity<String> entity = new HttpEntity<String>(json);
			ResponseEntity<String> resp = rest.exchange(URL_AUTENTICACAO, HttpMethod.POST, entity, String.class);
			
			if (RestUtil.isError(resp.getStatusCode())) {
				if (resp.getStatusCode() == HttpStatus.UNAUTHORIZED) {
					throw new BadCredentialsException("erro.username-ou-senha.invalido.mensagem");
				} else if (resp.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
					throw new AuthenticationServiceException("erro.sistema-indisponivel.mensagem");
				} else if (resp.getStatusCode() == HttpStatus.FORBIDDEN){
					throw new AuthenticationServiceException("erro.nao-autorizado.mensagem");
				} else {
					throw new AuthenticationServiceException("erro.sistema-indisponivel.mensagem");
				}
			} else {
				return Optional.of(resp.getHeaders());
			}
		} catch (RestClientException e) {
			throw new AuthenticationServiceException("erro.sistema-indisponivel.mensagem");
		}
				
	}

	private RestResponseErrorHandler getRestResponseErrorHandler() {
		return new RestResponseErrorHandler();
	}

	private String getJsonUsernameAndPassword(String username, String password) {
		String str = "{\"username\": \"%s\", \"password\": \"%s\"}";
		return String.format(str, username, password);
			
	}
		
}
