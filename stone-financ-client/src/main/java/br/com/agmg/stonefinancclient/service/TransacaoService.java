package br.com.agmg.stonefinancclient.service;

import java.util.List;

import br.com.agmg.stonefinancclient.dto.Transacao;

/**
 * 
 * Interface para as operações relacionadas a transacoes
 */
public interface TransacaoService {
		
	/**
	 * Recupera as transações de um usuário (username)
	 * @param cliente
	 * @return List<Transacao> lista de transações
	 */
	public List<Transacao> recuperarTransacoes(String username);

	/**
	 * Processar uma transação com cartão
	 * @param transacao
	 * @return Transacao processada
	 */
	public Transacao processarTransacao(Transacao transacao);
}
