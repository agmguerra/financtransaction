package br.com.agmg.stonefinancclient.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.agmg.stonefinancclient.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancclient.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancclient.validadores.DataExpiracaoValido;

/**
 * 
 * Representa o Cartão
 *
 */
public class Cartao {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	/**
	 * id do cartão
	 */
	private Long id;
	
	/**
	 * nome do dono no cartão
	 */
	@Size(min = 2, max = 100, message = "{erro.cartao.nome.size}")
	private String nome;
	
	/**
	 * Número do cartão
	 */
	@Size(min = 16, max = 16, message = "{erro.cartao.numero.size}")
	private String numero;
	
	/**
	 * Data de expiração do cartão
	 */
	@DataExpiracaoValido
	@Pattern(regexp="\\d\\d[/]\\d\\d", message="{erro.cartao.data-de-expiracao-formato.invalida}")
	private String dataDeExpiracao;
	
	/**
	 * Bandeira do cartão (VISA(0), MASTERCARD(1), etc)
	 */
	private BandeiraCartaoEnum bandeiraDoCartao;
		
	/**
	 * Tipo do cartão: 0 - chip, 1 - tarja magnética
	 */
	private TipoCartaoEnum tipoCartao;
	
	/**
	 * Se o tipo do cartão é CHIP então possuiPassword é true
	 */
	private Boolean possuiPassword;
	
	/**
	 * Código de segurança do cartao
	 */
	private String codigoSeguranca;
	
	/**
	 * password do cartão
	 */
	private String password;

	
	private Cliente cliente;
	
	public Cartao() {
		
	}
	
	public Cartao(Long id, String nome, String dataDeExpiracao, BandeiraCartaoEnum bandeiraCartao, 
			TipoCartaoEnum tipoCartao, Boolean possuiPassword, String codigoSegurancao, String password) {
		this.id = id;
		this.nome = nome;
		this.dataDeExpiracao = dataDeExpiracao;
		this.bandeiraDoCartao = bandeiraCartao;
		this.tipoCartao = tipoCartao;
		this.possuiPassword = possuiPassword;
		this.codigoSeguranca = codigoSegurancao;
		this.password = password;	
	}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDataDeExpiracao() {
		return dataDeExpiracao;
	}

	public void setDataDeExpiracao(String dataDeExpiracao) {
		this.dataDeExpiracao = dataDeExpiracao;
	}

	public BandeiraCartaoEnum getBandeiraDoCartao() {
		return bandeiraDoCartao;
	}

	public void setBandeiraDoCartao(BandeiraCartaoEnum bandeiraDoCartao) {
		this.bandeiraDoCartao = bandeiraDoCartao;
	}

	public TipoCartaoEnum getTipoCartao() {
		return tipoCartao;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public Boolean getPossuiPassword() {
		return possuiPassword;
	}

	public void setPossuiPassword(Boolean possuiPassword) {
		this.possuiPassword = possuiPassword;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setTipoCartao(TipoCartaoEnum tipoCartao) {
		this.tipoCartao = tipoCartao;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}
	

	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		
		final Cartao other = (Cartao) obj;
		if ( numero == null ) {
			if ( other.getNumero() != null ) {
				return false;
			}
		} else if( !numero.equals( other.getNumero() ) ) {
			return false;
		}
		
				
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( numero == null ) ? 0 : numero.hashCode() );
		return result;
	}

}
