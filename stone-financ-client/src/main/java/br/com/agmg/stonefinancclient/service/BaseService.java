package br.com.agmg.stonefinancclient.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import br.com.agmg.stonefinancclient.exception.RestResponseErrorHandler;
import br.com.agmg.stonefinancclient.seguranca.JwtContext;

/**
 * Classe que serve de base para todos os serviços
 * Prove a injeção de objetos necessários e métodos que normalmente
 * serão utilizados por qualquer serviço
 *
 */
public abstract class BaseService {
		
	@Autowired
	private JwtContext jwtContext;
	
	@Autowired
	private RestResponseErrorHandler restResponseErrorHandler;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private HttpServletRequest request;

	
	/**
	 * Insere o token Jwt no header e o parâmetro a ser enviado na consulta
	 * @param jwtContext
	 * @param obj
	 * @return
	 */
	protected <T> HttpEntity<T> getHttpEntityWithParamAndJwtToken (T obj) {
		
		//obtem o token jwt e insere no header para acessar api
	    HttpHeaders headers = jwtContext.getJwtHearders();
	    HttpEntity<T> entity = new HttpEntity<T>(obj, headers);	
	    
	    return entity;
	}
	
	/**
	 * Retorna o handler que verifica que algum erro foi retornado 
	 * a partir de uma requisição rest
	 * @return RestResponseErrorHandler
	 */
	protected RestResponseErrorHandler getRestResponseErroHandler() {
		return restResponseErrorHandler;
	}
	
	/**
	 * Retorna o objeto MessageSource que recupera as mensagens 
	 * do arquivo de properties
	 * @return MessageSource
	 */
	protected MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Retorna o HttpServletRequest
	 * @return HttpServletRequest
	 */
	protected HttpServletRequest getRequest() {
		return request;
	}
	
	/**
	 * Cria um objeto RestTemplate preparado para capturar os 
	 * erros
	 * @return RestTemplate
	 */
	protected RestTemplate createRestTemplate() {
		RestTemplate rest = new RestTemplate();
		rest.setErrorHandler(restResponseErrorHandler);
		
		return rest;
	}
	
	/**
	 * Retorna os Headers já contendo o Authorization com o Jwt
	 * @return
	 */
	protected HttpHeaders getHeadersWithJwt() {
		return jwtContext.getJwtHearders();
	}
}
