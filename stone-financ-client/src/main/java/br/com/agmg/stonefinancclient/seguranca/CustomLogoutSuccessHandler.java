package br.com.agmg.stonefinancclient.seguranca;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;
 
/**
 * 
 * Componente responsável por tratar os casos onde o logout 
 * aconteceu com sucesso
 *
 */
@Component
public class CustomLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

	//private static final ApplicationLogger logger = ApplicationLogger.getInstance();
	
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException,
			ServletException {
		
		
		removeCookies(request, response);
		setDefaultTargetUrl("/login?logout");
		super.onLogoutSuccess(request, response, authentication);
	}
	
	private void removeCookies(HttpServletRequest req, HttpServletResponse res) {
		Cookie[] cookies = req.getCookies();
		for (Cookie cookie: cookies) {
			cookie.setMaxAge(0);
			res.addCookie(cookie);
		}
	}
}
