package br.com.agmg.stonefinancclient.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.agmg.stonefinancclient.seguranca.CustomLoginSuccessHandler;
import br.com.agmg.stonefinancclient.seguranca.CustomLogoutSuccessHandler;
import br.com.agmg.stonefinancclient.seguranca.CustomRestAuthenticationProvider;
import br.com.agmg.stonefinancclient.seguranca.JWTAuthenticationFilter;
import br.com.agmg.stonefinancclient.seguranca.JWTAuthorizationFilter;

/**
 * 
 * Classe de configuração
 *
 */
@EnableWebSecurity
public class WebSecurityConfig {
	
	@Configuration
	public static class ConfiguracaoProtegida extends WebSecurityConfigurerAdapter {
		
		@Autowired
		CustomLoginSuccessHandler customLoginSuccessHandler;
		
		@Autowired
		CustomLogoutSuccessHandler customLogoutSuccessHandler;
			
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			
	        http
	        	.csrf().disable()
	        	.cors().disable()
	        	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	        	.and()
	        	.authorizeRequests()
	        		.antMatchers("/static/**","/webjars/**").permitAll()
	        		.antMatchers("/login").permitAll()
	        		.antMatchers("/registro").permitAll()
	         		.antMatchers("/app/**").authenticated()
	            	.antMatchers("/").authenticated()
	            	.and()
	            	.addFilterBefore(new JWTAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class)
	                .addFilter(new JWTAuthorizationFilter(authenticationManager()))                
	             .formLogin()
	            	.loginPage("/login")
	            	.permitAll()
	            	.successHandler(customLoginSuccessHandler)
	            	.and()
	            .logout()
	            	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	            	.logoutSuccessHandler(customLogoutSuccessHandler)
	            	.invalidateHttpSession(true)
	            	.deleteCookies("JSESSIONID")
	            	.permitAll();
	        
	        http.headers().cacheControl();
		}
		
		@Override
	    protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
			authManagerBuilder.authenticationProvider(new CustomRestAuthenticationProvider());
	    }
		
	}
 
}
