package br.com.agmg.stonefinancclient.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.agmg.stonefinancclient.dto.Cliente;
import br.com.agmg.stonefinancclient.exception.IntegrationException;
import br.com.agmg.stonefinancclient.utils.RestUtil;

/**
 * Classe de serviço para as funcionalidades de Cliente
 *
 */
@Service
public class ClienteServiceImpl extends BaseService implements ClienteService {

	@Value("${url.api.cliente}")
	private String URL_API; 
			
	
	/**
	 * @see br.com.agmg.stonefinancclient.service.ClienteService#cadastrarCliente(Cliente)
	 */
	@Override
	public Cliente cadastrarCliente(Cliente cliente) {	
		
		HttpEntity<Cliente> entity = new HttpEntity<Cliente>(cliente);
		
		RestTemplate rest = createRestTemplate();
	
		rest.setErrorHandler(getRestResponseErroHandler());
		ResponseEntity<String> resp = rest.postForEntity(URL_API, entity, String.class);

		Cliente clienteComId = cliente;
		if (!RestUtil.isError(resp.getStatusCode())) {
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				clienteComId = mapper.readValue(resp.getBody(), Cliente.class);
			} catch (Exception e) {
				//não tratar
			}
			
		} else {
			throw new IntegrationException(getMessageSource().getMessage("erro.cliente.nao-cadastrado-mensagem", null, getRequest().getLocale()));
		} 
		
		return clienteComId;
		
	}	
	

}
