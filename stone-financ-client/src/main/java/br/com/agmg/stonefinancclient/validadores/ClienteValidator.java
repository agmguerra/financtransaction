package br.com.agmg.stonefinancclient.validadores;

import java.util.InputMismatchException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.agmg.stonefinancclient.dto.Cliente;
import br.com.agmg.stonefinancclient.enumeration.TipoDocumentoEnum;
import br.com.agmg.stonefinancclient.utils.StringUtils;

/**
 * Classe validadora do documento do cliente (CPF e CNPJ)
 * a lógica de implementação dos validadores foi obtida através do site
 * @see http://www.devmedia.com.br/validando-o-cnpj-em-uma-aplicacao-java/22374
 * @see http://www.devmedia.com.br/validando-o-cpf-em-uma-aplicacao-java/22097
 * @author alexgmg
 *
 */
public class ClienteValidator implements ConstraintValidator<ClienteValido, Cliente>{

	
	private static final int DOC_MIN_SIZE = 11;
	private static final int DOC_MAX_SIZE = 20;
	
	
	
	/**
	 * valida o documento
	 */
	@Override
	public boolean isValid(Cliente cliente, ConstraintValidatorContext ctx) {
		
		boolean ret = true;
		if (cliente == null) {
			ctx.disableDefaultConstraintViolation();
	        ctx.buildConstraintViolationWithTemplate("{erro.cliente.invalido}")
	           .addConstraintViolation();

			ret = false;
		} else {
			
			if (cliente.getNome() == null) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cliente.nome.invalido}")
		        .addPropertyNode("nome").addConstraintViolation();
	
				ret = false;			
			} 
			
			if (cliente.getTipoDocumento() == null || (cliente.getTipoDocumento() != TipoDocumentoEnum.CPF
					&& cliente.getTipoDocumento() != TipoDocumentoEnum.CNPJ)) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cliente.tipo-documento.invalido}")
		        .addPropertyNode("tipoDocumento").addConstraintViolation();
	
				ret = false;
			} else if (cliente.getDocumento() == null || cliente.getDocumento().isEmpty() ||
					cliente.getDocumento().length() < DOC_MIN_SIZE || cliente.getDocumento().length() > DOC_MAX_SIZE) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cliente.documento.size}")
		           .addPropertyNode("documento").addConstraintViolation();
				
		        ret = false;							
			} else 	if (cliente.getTipoDocumento().equals(TipoDocumentoEnum.CPF) &&	!isCpfValido(cliente.getDocumento())) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cliente.cpf.invalido}")
		        .addPropertyNode("documento").addConstraintViolation();
		        
		        ret = false;
			} else if (cliente.getTipoDocumento().equals(TipoDocumentoEnum.CNPJ) && !isCnpjValido(cliente.getDocumento())) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cliente.cnpj.invalido}")
		        .addPropertyNode("documento").addConstraintViolation();
		        
		        ret = false;
			} 
					
			
		}
		return ret;
	}
	
	
	/**
	 * Valida o cpf
	 * @param cpf
	 * @return true or false
	 */
	private boolean isCpfValido(String cpfParm) {
		boolean ret = false;
		
		String cpf = StringUtils.retiraMascara(cpfParm);
		
		if (cpf.equals("00000000000") || cpf.equals("11111111111") ||
		    cpf.equals("22222222222") || cpf.equals("33333333333") ||
		    cpf.equals("44444444444") || cpf.equals("55555555555") ||
		    cpf.equals("66666666666") || cpf.equals("77777777777") ||
		    cpf.equals("88888888888") || cpf.equals("99999999999") ||
		    (cpf.length() != 11)) {
			ret = false;
		} else {
		    try {
		    	char dig10, dig11;
		        int sm, i, r, num, peso;
		        // Calculo do 1o. Digito Verificador
		        sm = 0;
		        peso = 10;
		        for (i=0; i<9; i++) {              
				// converte o i-esimo caractere do CPF em um numero:
				// por exemplo, transforma o caractere '0' no inteiro 0         
				// (48 eh a posicao de '0' na tabela ASCII)         
			        num = (int)(cpf.charAt(i) - 48); 
			        sm = sm + (num * peso);
			        peso = peso - 1;
		        }

		        r = 11 - (sm % 11);
		        if ((r == 10) || (r == 11)) {
		        	dig10 = '0';
		        } else { 
		        	dig10 = (char)(r + 48); // converte no respectivo caractere numerico
		        }

		        // Calculo do 2o. Digito Verificador
		        sm = 0;
		        peso = 11;
			    for(i=0; i<10; i++) {
			        num = (int)(cpf.charAt(i) - 48);
			        sm = sm + (num * peso);
			        peso = peso - 1;
			    }

			    r = 11 - (sm % 11);
			    if ((r == 10) || (r == 11)) {
			    	dig11 = '0';
			    } else { 
			    	dig11 = (char)(r + 48);
			    }

			    // Verifica se os digitos calculados conferem com os digitos informados.
			    if ((dig10 == cpf.charAt(9)) && (dig11 == cpf.charAt(10))) {
			    	ret = true;
			    } else {
			    	ret = false;
			    }
		    } catch (InputMismatchException EX) {
		        ret = false;
		    }
		}
		return ret;

	}

	/**
	 * Valida o cnpj
	 * @param cnpj
	 * @return true or false
	 */
	private boolean isCnpjValido(String cnpjParm) {
		
		String cnpj = StringUtils.retiraMascara(cnpjParm);
		
		boolean ret = false;
		
		// considera-se erro cnpj's formados por uma sequencia de numeros iguais
	    if (cnpj.equals("00000000000000") || cnpj.equals("11111111111111") ||
	        cnpj.equals("22222222222222") || cnpj.equals("33333333333333") ||
	        cnpj.equals("44444444444444") || cnpj.equals("55555555555555") ||
	        cnpj.equals("66666666666666") || cnpj.equals("77777777777777") ||
	        cnpj.equals("88888888888888") || cnpj.equals("99999999999999") ||
	       (cnpj.length() != 14)) {
	       ret = false;
	    } else {

		    char dig13, dig14;
		    int sm, i, r, num, peso;

			// "try" - protege o código para eventuais erros de conversao de tipo (int)
			try {
				// Calculo do 1o. Digito Verificador
			    sm = 0;
			    peso = 2;
			    for (i=11; i>=0; i--) {
					// converte o i-ésimo caractere do cnpj em um número:
					// por exemplo, transforma o caractere '0' no inteiro 0
					// (48 eh a posição de '0' na tabela ASCII)
			        num = (int)(cnpj.charAt(i) - 48);
			        sm = sm + (num * peso);
			        peso = peso + 1;
			        if (peso == 10)
			           peso = 2;
			    }

			    r = sm % 11;
			    if ((r == 0) || (r == 1)) {
			    	dig13 = '0';
			    } else { 
			    	dig13 = (char)((11-r) + 48);
			    }

			    // Calculo do 2o. Digito Verificador
		       sm = 0;
		       peso = 2;
		       for (i=12; i>=0; i--) {
			        num = (int)(cnpj.charAt(i)- 48);
			        sm = sm + (num * peso);
			        peso = peso + 1;
			        if (peso == 10) {
			           peso = 2;
			        }
		       }
 
		       r = sm % 11;
		       if ((r == 0) || (r == 1)) {
		          dig14 = '0';
		       } else { 
		    	   dig14 = (char)((11-r) + 48);
		       }

			   // Verifica se os dígitos calculados conferem com os dígitos informados.
			   if ((dig13 == cnpj.charAt(12)) && (dig14 == cnpj.charAt(13))) {
				   ret = true;
			   } else {
				   ret = false;
			   }
			} catch (InputMismatchException erro) {
			  ret = false;
			}
	    }
		
		return ret;
	}
}
