package br.com.agmg.stonefinancclient.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.com.agmg.stonefinancclient.seguranca.BaseInterceptor;

/**
 * 
 * Classe de configuração
 *
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	/**
	 * Adiciona um interceptor
	 */
    @Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new BaseInterceptor());
	}
    
 
    @Bean
    public static PropertyPlaceholderConfigurer properties(){
       PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
       Resource[] resources = new ClassPathResource[] { new ClassPathResource("application.properties") };
       ppc.setLocations( resources );
       ppc.setIgnoreUnresolvablePlaceholders( true );
       return ppc;
    }  

}