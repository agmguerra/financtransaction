package br.com.agmg.stonefinancclient.validadores;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Classe validadora da data de expiração
 *
 */
public class DataExpiracaoValidator implements ConstraintValidator<DataExpiracaoValido, String>{

	
	
	/**
	 * valida o documento
	 */
	@Override
	public boolean isValid(String dataExp, ConstraintValidatorContext ctx) {
		
		boolean ret = true;
		if (dataExp == null) {
			ctx.disableDefaultConstraintViolation();
	        ctx.buildConstraintViolationWithTemplate("{erro.cartao.data-de-expiracao.invalida}")
	        .addConstraintViolation();

			ret = false;
		} else {
			
			DateTimeFormatter formatter = null;
			YearMonth dtAnoMesExp = null;
			YearMonth dtAnoMesNow = YearMonth.now();
			if (dataExp.contains("/")) {
				formatter = DateTimeFormatter.ofPattern ( "MM/yy" );
			} else {
				formatter = DateTimeFormatter.ofPattern ( "MMyy" );
			}
			
			try {
				dtAnoMesExp = YearMonth.parse ( dataExp , formatter);
				if (dataExp == null || dataExp.isEmpty() || dtAnoMesExp.isBefore(dtAnoMesNow)) {
					ctx.disableDefaultConstraintViolation();
			        ctx.buildConstraintViolationWithTemplate("{erro.cartao.data-de-expiracao.invalida}")
			        .addConstraintViolation();		
					ret = false;
				}					

			} catch (Exception e) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cartao.data-de-expiracao.invalida}")
		        .addConstraintViolation();		
				ret = false;			
			}
			
			
		}
		return ret;
	}
	
	
}
