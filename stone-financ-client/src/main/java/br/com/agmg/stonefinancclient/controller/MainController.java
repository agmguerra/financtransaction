
package br.com.agmg.stonefinancclient.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.agmg.stonefinancclient.dto.Cliente;
import br.com.agmg.stonefinancclient.dto.Transacao;
import br.com.agmg.stonefinancclient.service.ClienteService;
import br.com.agmg.stonefinancclient.service.TransacaoService;

/**
 * Controlador da aplicação
 *
 */
@Controller
@RequestMapping("/")
public class MainController {
	
	@Autowired
	private TransacaoService transacaoService;
	
	@Autowired
	private ClienteService clienteService;	
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView home() {
		return new ModelAndView("login");
	}

	
	/**
	 * Exibe a Tela de registro de usuário
	 * @param cliente
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "registro", method = RequestMethod.GET)
	public ModelAndView exibirRegistrarUsuario(@ModelAttribute(value="cliente") Cliente cliente, HttpServletRequest request) {		
		return new ModelAndView("registro");
	}
	
	/**
	 * registra o usuário
	 * @param cliente
	 * @param result
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "registro", method = RequestMethod.POST)
	public ModelAndView registrarUsuario(@ModelAttribute(value="cliente") @Valid Cliente cliente, BindingResult result, HttpServletRequest request) {
		
		ModelAndView mav = null;
		
		if (result.hasErrors()) {
            mav =  new ModelAndView("registro");
        } else {		
			try {
				Cliente clienteGracado = clienteService.cadastrarCliente(cliente);
				
				mav = new ModelAndView("registro_ok");
				mav.addObject("success", true);
				mav.addObject("cliente", clienteGracado);
			} catch (Exception e) {
				mav =  new ModelAndView("registro");
				mav.addObject("erro", true);
			}
        }
		return mav;
	}
	
	/**
	 * Exibe a tela de login
	 * @return
	 */
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView("login");
	}
	
	
	@RequestMapping(value = "app", method = RequestMethod.GET)
	public ModelAndView index() {
		return new ModelAndView("index");
	}
	
	/**
	 * Exibir tela de cartão
	 * @return ModelAndView
	 */
	@RequestMapping(value = "app/transacao", method = RequestMethod.GET)
	public ModelAndView exibirProcessarTransacao(@ModelAttribute(value="transacao") Transacao transacao, HttpServletRequest request) {
		return new ModelAndView("transacao");
	}
	
	/**
	 * Processar a transacao informada
	 * @return
	 */
	@RequestMapping(value = "app/transacao", method = RequestMethod.POST)
	public ModelAndView processarTransacao(@ModelAttribute(value="transacao") @Valid Transacao transacao, BindingResult result, HttpServletRequest request) {
		
		
		ModelAndView mav = null;
		
		if (result.hasErrors()) {
            mav =  new ModelAndView("transacao");
        } else {		
			try {
								
				transacaoService.processarTransacao(transacao);
				
				mav = new ModelAndView("transacao");
				mav.addObject("success", true);
				mav.addObject("transacao", new Transacao());
			} catch (Exception e) {
				mav =  new ModelAndView("transacao");
				mav.addObject("erro", true);
			}
        }
		return mav;

	}

	
	/**
	 * Lista as transações po username
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "app/listartransacoes", method = RequestMethod.GET)
	public ModelAndView listarTransacoes(HttpServletRequest request) {
		
		List<Transacao> transacoes = new ArrayList<Transacao>();
				
		String username = request.getUserPrincipal().getName();
		
		if (username != null && !username.isEmpty()) {
		
			transacoes = transacaoService.recuperarTransacoes(username);
		}
		
		ModelAndView mav = new ModelAndView("listartransacoes");
        mav.addObject("transacoes", transacoes);
		
		return mav;
	}

	
}
