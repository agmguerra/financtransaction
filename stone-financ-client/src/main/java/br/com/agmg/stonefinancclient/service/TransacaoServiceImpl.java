package br.com.agmg.stonefinancclient.service;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.agmg.stonefinancclient.dto.Transacao;
import br.com.agmg.stonefinancclient.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancclient.exception.IntegrationException;
import br.com.agmg.stonefinancclient.utils.RestUtil;

/**
 * Implementa a interface de operações com as transações de cartões
 *
 */
@Service
public class TransacaoServiceImpl extends BaseService implements TransacaoService {

	@Value("${url.api.transacao}")
	private String URL_API; 
	
	
	/**
	 * @see br.com.agmg.stonefinancclient.service.TransacaoService#recuperarTransacoes(String)
	 */
	@Override
	public List<Transacao> recuperarTransacoes(String username) {
		
		try {
	
			
		    HttpHeaders headers = getHeadersWithJwt();
		    HttpEntity<String> entity = new HttpEntity<String>(username, headers);
		    
			RestTemplate rest = new RestTemplate();
			
			rest.setErrorHandler(getRestResponseErroHandler());
			ResponseEntity<String> resp = rest.exchange(URL_API + "?username=" + username, HttpMethod.GET, entity, String.class);

								
			if (RestUtil.isError(resp.getStatusCode())) {
				if (resp.getStatusCode() == HttpStatus.UNAUTHORIZED) {
					throw new BadCredentialsException("erro.username-ou-senha.invalido.mensagem");
				} else if (resp.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
					throw new IntegrationException("erro.sistema-indisponivel.mensagem");
				} else if (resp.getStatusCode() == HttpStatus.FORBIDDEN){
					throw new BadCredentialsException("erro.nao-autorizado.mensagem");
				} else {
					throw new IntegrationException("erro.sistema-indisponivel.mensagem");
				}
			} else {
				ObjectMapper mapper = new ObjectMapper();
				JsonNode root = mapper.readTree(resp.getBody());
				root = root.findPath("transacoes");
				List<Transacao> transacoes = null;
				if (root.isArray()) {		
					transacoes = mapper.readValue(root.toString(), new TypeReference<List<Transacao>>(){});
				}

				return transacoes;
			}
		} catch (RestClientException | IOException e) {
			throw new IntegrationException("erro.transacoes-nao-encontradas");
		}
		
	}


	@Override
	public Transacao processarTransacao(Transacao transacao) {
		try {
			
			//Configura o tipo de cartão para chip como default e não possui password
			transacao.getCartao().setTipoCartao(TipoCartaoEnum.CHIP);
			transacao.getCartao().setPossuiPassword(false);

			
		    HttpHeaders headers = getHeadersWithJwt();
		    HttpEntity<Transacao> entity = new HttpEntity<Transacao>(transacao, headers);
		    
			RestTemplate rest = new RestTemplate();
			
			rest.setErrorHandler(getRestResponseErroHandler());
			ResponseEntity<String> resp = rest.exchange(URL_API, HttpMethod.POST, entity, String.class);
								
			if (RestUtil.isError(resp.getStatusCode())) {
				if (resp.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
					throw new IntegrationException("erro.transacao.invalida");
				} if (resp.getStatusCode() == HttpStatus.UNAUTHORIZED) {
					throw new BadCredentialsException("erro.transacao.invalida");
				} else if (resp.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
					throw new IntegrationException("erro.sistema-indisponivel.mensagem");
				} else if (resp.getStatusCode() == HttpStatus.FORBIDDEN){
					throw new IntegrationException("erro.nao-autorizado.mensagem");
				} else {
					throw new IntegrationException("erro.sistema-indisponivel.mensagem");
				}
			} else if (resp.getStatusCode().equals(HttpStatus.CREATED)){
				
				//Retorna a transacao com id
				URI location = resp.getHeaders().getLocation();
				String[] parts = location.getPath().split("/");
				Long trId = Long.valueOf(parts[parts.length-1]);
				transacao.setId(trId);
			}
		} catch (RestClientException e) {
			throw new IntegrationException("erro.transacoes-nao-encontradas");
		}
		
		return transacao;
	}
	
	

}
