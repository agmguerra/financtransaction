package br.com.agmg.stonefinancclient.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Representa os valores de tipo de transação 
 *
 */
public enum TipoTransacaoEnum {
		
	DEBITO(0), CREDITO(1);

	Integer valor;
	
	TipoTransacaoEnum(int valor) {
		this.valor = valor;
	}

	public Integer getValue() {
		return valor;
	}
	
	/**
	 * Cria o Enum a partir de um Json. Se o valor
	 * não estiver no domínio retorna null
	 * @param param valor informado do  tipo da transação
	 * @return Enum referente ao tipo informado
	 */
	@JsonCreator
    public static TipoTransacaoEnum create (String param) {
		TipoTransacaoEnum ret = null;
        if(param != null) {
            for(TipoTransacaoEnum tipo : values()) {
                if(param.equals(tipo.getValue()) || param.equals(tipo.name())) {
                    ret = tipo;
                }
            }
        }
        return ret;
    }

}
