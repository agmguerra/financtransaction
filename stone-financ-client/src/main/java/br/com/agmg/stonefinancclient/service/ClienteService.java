package br.com.agmg.stonefinancclient.service;

import br.com.agmg.stonefinancclient.dto.Cliente;

/**
 * 
 * Interface para as operações relacionadas a clientes
 */
public interface ClienteService {
		
	/**
	 * Cadastra um cliente
	 * @param cliente
	 * @return Cliente cadastrado
	 */
	public Cliente cadastrarCliente(Cliente cliente);

}
