package br.com.agmg.stonefinancclient.validadores;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target({ElementType.FIELD, ElementType.TYPE_PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DataExpiracaoValidator.class)
public @interface DataExpiracaoValido {
	
	String message() default "{erro.cartao.data-de-expiracao}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}
