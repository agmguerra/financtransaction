package br.com.agmg.stonefinancclient.seguranca;

import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.SECRET;
import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.TOKEN_PREFIX;
import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.HEADER_STRING;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.Jwts;

/**
 * 
 * Filtro responsável por inteceptar todos requests das
 * urls restritas e verificar se existe o Jwt token de 
 * autenticação
 *
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	public JWTAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {


        Optional<String> cookieValue = getAuthorizationCookieValue(req.getCookies());
        

        if (!cookieValue.isPresent() || !cookieValue.get().startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(cookieValue.get());

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String jwtToken) {
    	UsernamePasswordAuthenticationToken userPassAuth = null;
        if (jwtToken != null) {
            // parse the token.
            String user = Jwts.parser()
                    .setSigningKey(SECRET.getBytes())
                    .parseClaimsJws(jwtToken.replace(TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();

            if (user != null) {
            	userPassAuth = new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
            }
        }
        return userPassAuth;
    }
    
	private Optional<String> getAuthorizationCookieValue(Cookie[] cookies) {
		Optional<String> cookieValue = Optional.empty();
		try {
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals(HEADER_STRING)) {
						cookieValue = Optional.of(URLDecoder.decode(cookie.getValue(), "UTF-8"));
						break;
					}
				}
			}
		} catch (UnsupportedEncodingException e) {
			//TODO ver qual exception colocar
		}
		return cookieValue;
	}
}
