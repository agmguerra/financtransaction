package br.com.agmg.stonefinancclient.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.agmg.stonefinancclient.enumeration.TipoDocumentoEnum;
import br.com.agmg.stonefinancclient.validadores.ClienteValido;

/**
 * 
 * Representa o Cliente
 *
 */
@ClienteValido
public class Cliente {
	
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	/**
	 * id do cliente
	 */
	private Long id;

	/**
	 * nome do cliente
	 */
	@Size(min = 2, max = 100, message = "{erro.cliente.nome.size}")
	private String nome;
	
	@Size(min = 4, max = 30, message = "{erro.cliente.username.size}")
	private String username;
	
	@Size(min = 4, max = 30, message = "{erro.cliente.password.size}")
	private String password;	
	
	@NotNull(message = "{erro.cliente.tipo.tipodocumento.notnull}")
	private TipoDocumentoEnum tipoDocumento;
	
	@Size(min = 11, max = 14, message = "{erro.cliente.documento.size}")
	private String documento;
 	
	private byte[] privateKey;
	
	private byte[] publicKey;

	private List<Cartao> cartoes;

	
	public Cliente() {
		
	}
	
	public Cliente(Long id, String nome, TipoDocumentoEnum tipoDocumento, String documento, String username, String password) {
		this.id = id;
		this.nome = nome;
		this.tipoDocumento = tipoDocumento;
		this.documento = documento;
		this.username = username;
		this.password = password;
	}
	
	public Long getId() {
		return id;
	}

	
	public TipoDocumentoEnum getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoEnum tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cartao> getCartoes() {
		return cartoes;
	}

	public void setCartoes(List<Cartao> cartoes) {
		this.cartoes = cartoes;
	}
	
	public byte[] getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(byte[] privateKey) {
		this.privateKey = privateKey;
	}

	public byte[] getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(byte[] publicKey) {
		this.publicKey = publicKey;
	}
	
	
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		
		final Cliente other = (Cliente) obj;
		if ( documento == null ) {
			if ( other.getDocumento() != null ) {
				return false;
			}
		} else if( !documento.equals( other.getDocumento() ) ) {
			return false;
		}
		
				
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( documento == null ) ? 0 : documento.hashCode() );
		return result;
	}

}
