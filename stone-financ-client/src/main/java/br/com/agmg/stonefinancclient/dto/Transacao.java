package br.com.agmg.stonefinancclient.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.Valid;

import br.com.agmg.stonefinancclient.enumeration.TipoTransacaoEnum;

/**
 * 
 * Representa a transação
 *
 */
public class Transacao {
	
	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	/**
	 * id da transacao
	 */
	private Long id;

	/**
	 * Data da transação
	 */
	private Date dataTransacao;
	
	/**
	 * Valor da transação
	 */
	private BigDecimal valor;
	
	/**
	 * Tipo de transação (DEBITO - 0 CREDITO - 1)
	 */
	private TipoTransacaoEnum tipoTransacao;
	
	/**
	 * Número de parcelas da transação se o tipo for CRÉDITO
	 */
	private Integer numeroParcelas;
	
	/**
	 * Cartao associado a transação
	 */
	@Valid
	private Cartao cartao;
	
	public Transacao() {
		
	}
	
	public Transacao(Long id, Date dataTransacao, TipoTransacaoEnum tipoTransacao, Integer numParcelas, BigDecimal valor, Cartao cartao) {
		this.id = id;
		this.dataTransacao = dataTransacao;
		this.numeroParcelas = numParcelas;
		this.tipoTransacao = tipoTransacao;
		this.valor = valor;
		this.cartao = cartao;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataTransacao() {
		return dataTransacao;
	}

	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public TipoTransacaoEnum getTipoTransacao() {
		return tipoTransacao;
	}

	public void setTipoTransacao(TipoTransacaoEnum tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}

	public Cartao getCartao() {
		return cartao;
	}

	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}
	
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		
		final Transacao other = (Transacao) obj;
		if ( dataTransacao == null ) {
			if ( other.getDataTransacao() != null ) {
				return false;
			}
		} else if( !dataTransacao.equals( other.getDataTransacao() ) ) {
			return false;
		}
		
		if (cartao == null) {
			if (other.getCartao() != null) {
				return false;
			}
		} else if (!cartao.equals(other.getCartao())) {
			return false;
		}
		
		if (valor == null) {
			if (other.getValor() != null) {
				return false;
			}
		} else if (!valor.equals(other.getValor())) {
			return false;
		}
		
				
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( dataTransacao == null ) ? 0 : dataTransacao.hashCode() );
		result = prime * result + ( ( cartao == null ) ? 0 : cartao.hashCode() );
		return result;
	}

}
