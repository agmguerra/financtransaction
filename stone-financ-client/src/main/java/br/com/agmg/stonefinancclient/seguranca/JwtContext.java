package br.com.agmg.stonefinancclient.seguranca;

import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import br.com.agmg.stonefinancclient.utils.JwtUtils;

import static br.com.agmg.stonefinancclient.seguranca.SecurityConstants.HEADER_STRING;;

@Component
public class JwtContext {

	@Autowired
	private HttpServletRequest request;
	
	public HttpHeaders getJwtHearders() {
		
		Optional<String> authParam = JwtUtils.getAuthorizationParam(request.getCookies());
		HttpHeaders headers = null;
		if (authParam.isPresent()) {
			headers = new HttpHeaders();
	        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	        headers.set(HEADER_STRING, authParam.get());
		}
		
		return headers;
		
		
	}
	
}
