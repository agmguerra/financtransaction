
var restApiLocation = 'http://localhost:8080';

var loadChamados = function() {
	
	$.ajax({ 
	   type: "GET",
	   url: $(location).attr('protocol') + "/sac/api/chamados/search/findAllByOrderByDataAtendDescEstadoAsc",
	   success: function(result){ 			 
			var newRows = "<tbody>";
			for (var i = 0; i < result._embedded.chamados.length; i++) {
			   newRows += "<tr><td>" + result._embedded.chamados[i].dataAtend + "</td>" + 
			   			  "<td>" + result._embedded.chamados[i].estado.nome + "</td>" + 
			   			  "<td>" + result._embedded.chamados[i].tipoChamado.nome + "</td>" + 
			   			  "<td>" + result._embedded.chamados[i].motivoChamado.nome + "</td>" +
			   			  "<td>" + result._embedded.chamados[i].descricao + "</td></tr>"
			};
			newRows += "</tbody>";
			$("table thead").after(newRows);
		 		 
	   },
	   erro: function(jqXHR, textStatus, errorThrown) {
	    	$.notify({
	    		title: 'Erro! ',
	    		message: 'Não foi possível obter a lista de chamados'
	    	},{
	    		type: 'danger',
	    		delay: 1000,
	    	});
	   },
	});
};

var loadEstados = function() {
	
	$.ajax({		
		type:'GET',
	    url: $(location).attr('protocol') + "/sac/api/estados",	    
	    dataType: 'json',
	    success: function( result ) {
			for (var i = 0; i < result._embedded.estados.length; i++) {
				$('#estadoSel').append($('<option>').
						text(result._embedded.estados[i].nome)
						.attr('value', result._embedded.estados[i]._links.self.href));
			};    	
	    }
	});

};

var loadTiposChamados = function() {
	
	$.ajax({		
		type:'GET',
	    url: $(location).attr('protocol') + "/sac/api/tiposchamados",	    
	    dataType: 'json',
	    success: function( result ) {
			for (var i = 0; i < result._embedded.tiposchamados.length; i++) {
				$('#tipoChamadoSel').append($('<option>').
						text(result._embedded.tiposchamados[i].nome)
						.attr('value', result._embedded.tiposchamados[i]._links.self.href));
			};    	
	    }
	});
	
};

var loadMotivosChamados = function() {
	
	$.ajax({		
		type:'GET',
	    url: $(location).attr('protocol') + "/sac/api/motivoschamados",	    
	    dataType: 'json',
	    success: function( result ) {
			for (var i = 0; i < result._embedded.motivoschamados.length; i++) {
				$('#motivoChamadoSel').append($('<option>').
						text(result._embedded.motivoschamados[i].nome)
						.attr('value', result._embedded.motivoschamados[i]._links.self.href));
			};    	
	    }
	});
	
};

var cadastrarChamado = function() {
	
	var chamado = '{"descricao": "' + $("#descricaoText").val() + '",' +
	                '"estado": "' + $("#estadoSel").val() + '",' +
	                '"motivoChamado": "' + $("#motivoChamadoSel").val() + '",' +
	                '"tipoChamado": "' + $("#tipoChamadoSel").val() + '"}'
	
	
	$.ajax({		
		type:'POST',
	    url: $(location).attr('protocol') + "/sac/api/chamados",
	    contentType: "application/json",
	    dataType: 'json',
	    data: chamado,
	    success: function(result) {
	    	$.notify({
	    		title: 'Sucesso! ',
	    		message: 'Chamado cadastrado com sucesso'
	    	},{
	    		type: 'success',
	    		delay: 1000,
	    	});
	    	$("#descricaoText").val("");
	    	$("#estadoSel").val("");
			$("#motivoChamadoSel").val("");
			$("#tipoChamadoSel").val("");
	    },
	    error: function() {
	    	$.notify({
	    		title: 'Erro! ',
	    		message: 'Não foi possível cadastrar o chamado'
	    	},{
	    		type: 'danger',
	    		delay: 1000,
	    	});
	    }
	});
	
var executarLogin = function() {
	
	var credentials = '{"username": "' + $("#username").val() + '",' +
	                    '"password": "' + $("#password").val() + '"}'
	$.ajax({		
		type:'POST',
	    url: restApiLocation + "/login",
	    contentType: "application/json",
	    dataType: 'json',
	    data: credentials,
	    statusCode: {
	        401: function() {
	          alert( "não logou" );
	        },
	        403: function() {
	        	alert("não logou");
	        },
	        200: function() {
	        	alert("logou");
	        }
	     }
	  
	});
}
	
}

