package br.com.agmg.stonefinancclient.testes.controller;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import br.com.agmg.stonefinancclient.StoneFinancClientApplication;
import br.com.agmg.stonefinancclient.controller.MainController;
import br.com.agmg.stonefinancclient.dto.Cartao;
import br.com.agmg.stonefinancclient.dto.Cliente;
import br.com.agmg.stonefinancclient.dto.Transacao;
import br.com.agmg.stonefinancclient.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancclient.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancclient.enumeration.TipoDocumentoEnum;
import br.com.agmg.stonefinancclient.enumeration.TipoTransacaoEnum;
import br.com.agmg.stonefinancclient.service.ClienteService;
import br.com.agmg.stonefinancclient.service.TransacaoService;



/**
 * Classe para teste de ClienteoServicos
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoneFinancClientApplication.class)
public class MainControllerTests {

	@TestConfiguration
	static class ClienteServiceImplTestContextConfiguration {

		@Bean
		public MainController mainController() {
			return new MainController();
		}
		
	}

	@TestConfiguration
	static class PrincipalContextConfiguration implements Principal {

		@Override
		public String getName() {
			
			return "teste";
		}
		
	}
	
		
	private BindingResult result;
	
	@Autowired
	private MainController mainController;
	
	@MockBean
	private ClienteService clienteService;
	
	@MockBean
	private TransacaoService transacaoService; 
	
	
    private Cliente clienteSemId = null;
    private Cliente cliente = null;
    private List<Transacao> transacoes = null;
    private Transacao transacao = null;
    
	
	
	@Before
	public void preparaTestes() {
		
		clienteSemId = new Cliente(null, "Marcos da Silva", TipoDocumentoEnum.CPF, "71025626834", "marcos", "1234");
		cliente = new Cliente();
		BeanUtils.copyProperties(clienteSemId, cliente);
		cliente.setId(10L);
		
		Mockito.when(clienteService.cadastrarCliente(clienteSemId))
	      .thenReturn((cliente));
		
		Cartao cartao = new Cartao(1L, "João da Silva", "12/18", BandeiraCartaoEnum.VISA, TipoCartaoEnum.CHIP, false, "111", "1234");
		transacao = new Transacao(100L, new Date(), TipoTransacaoEnum.CREDITO, 0, new BigDecimal(100), cartao);
		transacoes = new ArrayList<Transacao>();
		transacoes.add(transacao);
		
		
		Mockito.when(transacaoService.recuperarTransacoes("teste"))
		  .thenReturn(transacoes);
		
		
		Mockito.when(transacaoService
				.processarTransacao(new Transacao(null, new Date(), TipoTransacaoEnum.CREDITO, 0, new BigDecimal(100), cartao)))
		.thenReturn(transacao);
		
		
	}

	@Test
	public void testarRegistrarClienteOk() {
		
		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		result = Mockito.mock(BindingResult.class);
	    Mockito.when(result.hasErrors()).thenReturn(false);
		
		ModelAndView mav = mainController.registrarUsuario(cliente, result, request);
		
		Boolean result  = (Boolean)mav.getModel().get("success");
		
		assertTrue(result);
		assertEquals("registro_ok", mav.getViewName());
		
	}
	
	@Test
	public void testarRegistrarClienteErroValid() {
		
		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		result = Mockito.mock(BindingResult.class);
	    Mockito.when(result.hasErrors()).thenReturn(true);
		
		ModelAndView mav = mainController.registrarUsuario(cliente, result, request);
				
		assertEquals("registro", mav.getViewName());

	}
	
	
	@Test
	public void testarLitarTransacoesByUsername() {
				
		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);

		Mockito.when(request.getUserPrincipal()).thenReturn(new PrincipalContextConfiguration());

		
		ModelAndView mav = mainController.listarTransacoes(request);
		
		@SuppressWarnings("unchecked")
		List<Transacao> lista  = (List<Transacao>)mav.getModel().get("transacoes");
		

		assertEquals("listartransacoes", mav.getViewName());
		assertNotNull(lista.get(0));
	}

	@Test
	public void testarProcessarTransacaoOk() {
		
		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		result = Mockito.mock(BindingResult.class);
	    Mockito.when(result.hasErrors()).thenReturn(false);
	    
	    ModelAndView mav = mainController.processarTransacao(transacao, result, request);

	    Boolean result  = (Boolean)mav.getModel().get("success");
		
		assertTrue(result);
		assertEquals("transacao", mav.getViewName());
		assertNotNull(transacao.getId());

	}
	
}
