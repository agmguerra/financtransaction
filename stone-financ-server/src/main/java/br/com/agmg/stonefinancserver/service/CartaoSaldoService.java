package br.com.agmg.stonefinancserver.service;

import java.math.BigDecimal;

import br.com.agmg.stonefinancserver.entity.CartaoSaldo;
import br.com.agmg.stonefinancserver.entity.Transacao;

/**
 * Interface de serviços para tratamento de
 * saldo de cartões do cliene
 *
 */
public interface CartaoSaldoService {

	/**
	 * Verifica se o cartao possui saldo para a operação
	 * @param cartaoBusca
	 * @return true se tem saldo e false se não tem
	 */
	public Boolean isCartaoValido(String numero, String nome, String dataExpiracao, String codigoSegurancao, BigDecimal valor, String password);	
	
	/**
	 * Atualiza o saldo do cartao
	 * @param cartaoSaldo
	 * @return CartaoSaldo
	 */
	public CartaoSaldo atualizarSaldoCartao(Transacao transacao);
	
	
}