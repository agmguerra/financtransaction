package br.com.agmg.stonefinancserver.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Representa os possíveis valores de bandeiras de cartão 
 *
 */
public enum BandeiraCartaoEnum {
		
	VISA(0), MASTERCARD(1);

	Integer valor;
	
	BandeiraCartaoEnum(int valor) {
		this.valor = valor;
	}

	public Integer getValue() {
		return valor;
	}
	
	/**
	 * Cria o Enum a partir de um Json. Se o valor
	 * não estiver no domínio retorna null
	 * @param param valor informado da bandeira do cartão
	 * @return Enum referente a bandeira informada
	 */
	@JsonCreator
    public static BandeiraCartaoEnum create (String param) {
		BandeiraCartaoEnum ret = null;
        if(param != null) {
            for(BandeiraCartaoEnum tipo : values()) {
                if(param.equals(tipo.getValue()) || param.equals(tipo.name())) {
                    ret = tipo;
                }
            }
        }
        return ret;
    }

}
