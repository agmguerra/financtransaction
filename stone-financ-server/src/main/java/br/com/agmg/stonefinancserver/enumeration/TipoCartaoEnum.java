package br.com.agmg.stonefinancserver.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Representa o domínio dos tipos de cartões possíveis
 * 
 */
public enum TipoCartaoEnum {
		
	CHIP(0), TARJA(1);

	Integer valor;
	
	TipoCartaoEnum(int valor) {
		this.valor = valor;
	}

	public Integer getValue() {
		return valor;
	}
	
	/**
	 * Cria o Enum a partir de um Json. Se o valor
	 * não estiver no domínio retorna null
	 * @param param valor informado do  tipo do cartão
	 * @return Enum referente ao tipo informada
	 */
	@JsonCreator
    public static TipoCartaoEnum create (String param) {
		TipoCartaoEnum ret = null;
        if(param != null) {
            for(TipoCartaoEnum tipo : values()) {
                if(param.equals(tipo.getValue()) || param.equals(tipo.name())) {
                    ret = tipo;
                }
            }
        }
        return ret;
    }

}
