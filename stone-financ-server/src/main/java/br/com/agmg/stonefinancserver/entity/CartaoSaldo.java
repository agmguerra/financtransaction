package br.com.agmg.stonefinancserver.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import io.swagger.annotations.ApiModel;

/**
 * Classe que armazena os saldos dos cartões.
 * 
 * @author alexgmg
 *
 */
@ApiModel(description="Contém informações dos saldos dos cartões um cartão de um cliente")
@Entity
@Table(name = "cartao_saldo")
public class CartaoSaldo {
	
	/**
	 * id do cartão
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "cartao_id", unique = true, nullable = false)
	private Long id;
	
	/**
	 * nome do dono no cartão
	 */
	@Column(name = "nome_cartao", nullable = false, length=60)
	private String nome;
	
	/**
	 * Número do cartão
	 */
	@Column(name = "numero_cartao", unique = true, nullable = false)
	private String numero;
	
	/**
	 * Data de expiração do cartão
	 */
	@Column(name = "dt_expiracao", nullable = false)
	private String dataDeExpiracao;
	
	/**
	 * Bandeira do cartão (VISA(0), MASTERCARD(1), etc)
	 */
	@Column(name = "bandeira_cartao", nullable = false)
	private BandeiraCartaoEnum bandeiraDoCartao;
		
	/**
	 * Tipo do cartão: 0 - chip, 1 - tarja magnética
	 */
	@Column(name = "tp_cartao", nullable = false)
	private TipoCartaoEnum tipoCartao;
	
	/**
	 * Se o tipo do cartão é CHIP então possuiPassword é true
	 */
	@Column(name = "possui_password", nullable = false)
	private Boolean possuiPassword;
	
	/**
	 * Código de segurança do cartao
	 */
	@Column(name = "cod_seg", nullable = true)
	private String codigoSeguranca;
	
	/**
	 * password do cartão
	 */
	@Column(name = "password", nullable = false, length=100)
	@JsonIgnore
	private String password;

	@Column(name = "saldo", nullable = true)
	@JsonIgnore
	private BigDecimal saldo;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDataDeExpiracao() {
		return dataDeExpiracao;
	}

	public void setDataDeExpiracao(String dataDeExpiracao) {
		this.dataDeExpiracao = dataDeExpiracao;
	}

	public BandeiraCartaoEnum getBandeiraDoCartao() {
		return bandeiraDoCartao;
	}

	public void setBandeiraDoCartao(BandeiraCartaoEnum bandeiraDoCartao) {
		this.bandeiraDoCartao = bandeiraDoCartao;
	}

	public TipoCartaoEnum getTipoCartao() {
		return tipoCartao;
	}

	public void setTipoCartao(TipoCartaoEnum tipoCartao) {
		this.tipoCartao = tipoCartao;
	}

	public Boolean getPossuiPassword() {
		return possuiPassword;
	}

	public void setPossuiPassword(Boolean possuiPassword) {
		this.possuiPassword = possuiPassword;
	}

	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}

	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

}
