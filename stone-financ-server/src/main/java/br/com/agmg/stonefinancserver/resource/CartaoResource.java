package br.com.agmg.stonefinancserver.resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.exception.NotFoundException;
import br.com.agmg.stonefinancserver.service.CartaoService;


@RestController
public class CartaoResource {

	@Autowired
	private ClienteResource clienteResource;
	
	@Autowired
	private CartaoService cartaoService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private HttpServletRequest request;
	
	
	@GetMapping("/api/cartoes")
	public ResponseEntity<Resources<Resource<Cartao>>> recuperarTodosCartoes() {
		
		List<Cartao> cartoes = cartaoService.recuperarTodosCartoes();
		List<Resource<Cartao>> resourcesList = cartoes.stream().map(cartao -> cartaoToResource(cartao)).collect(Collectors.toList());
		
		Resources<Resource<Cartao>> resources = new Resources<Resource<Cartao>>(resourcesList);
		
		return ResponseEntity.ok(resources);
	}

	
	@GetMapping("/api/cartoes/{id}")
	public ResponseEntity<Resource<Cartao>> recuperarCartao(@PathVariable Long id) {
		Optional<Cartao> cartao = cartaoService.recuperarCartao(id); 
		if (!cartao.isPresent()) {
			throw new NotFoundException(messageSource.getMessage("erro.cliente.nao-encontrado.mensagem", null, request.getLocale()));
		}
		
		//Colocar o link para recuperar todos os usuários
		//retrieveAllUsers seguindo o padrão HATEOAS
		Resource<Cartao> resource = new Resource<Cartao>(cartao.get());
		ControllerLinkBuilder linkTo = linkTo(methodOn(clienteResource.getClass()).recuperarCliente(cartao.get().getId()));
		
		resource.add(linkTo.withRel("cliente"));
		
		return ResponseEntity.ok(resource);	
	}
	
	@PostMapping("/api/cartoes")
	public ResponseEntity<Object> gravarCartao(@Valid @RequestBody Cartao cartao) {
		Cartao cartaoGravado = cartaoService.gravarCartao(cartao);
		
		URI location = ServletUriComponentsBuilder
							.fromCurrentRequest()
							.path("/{id}")
							.buildAndExpand(cartaoGravado.getId()).toUri();
		
		ResponseEntity<Object> ret = ResponseEntity.created(location).build();
	
		return ret;
	}

	@DeleteMapping("/api/cartoes/{id}")
	public void excluirCartao(@PathVariable Long id) {
		cartaoService.excluirCartao(id);
	}

	private Resource<Cartao> cartaoToResource(Cartao cartao) {
		
		//Colocar o link para recuperar todos os usuários
				//retrieveAllUsers seguindo o padrão HATEOAS
		Resource<Cartao> resource = new Resource<Cartao>(cartao);
		ControllerLinkBuilder linkTo = linkTo(methodOn(clienteResource.getClass()).recuperarCliente(cartao.getId()));
		resource.add(linkTo.withRel("cliente"));
				 
		return resource;
		 
	}
	
	
}
