package br.com.agmg.stonefinancserver.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.agmg.stonefinancserver.entity.Cliente;

public interface ClienteService {

	/**
	 * Recuperar todos os clientes
	 * 
	 * @return List<Cliente> - lists de clientes
	 */
	public List<Cliente> recuperarTodosClientes();
	
	/**
	 * Recuperar os clintes por pagina por id
	 * @param pagina número da página que se quer recuperar
	 * @param tamanhoPagina número de objetos Cliente da página que ser quer recuperar
	 * @return
	 */
	public Page<Cliente> recuperarClientes(Integer pagina, Integer tamanhoPagina);
	
	/**
	 * Recupera um clientes através de seu id
	 * 
	 * @param id do cliente
	 * @return Cliente objeto com as informações de cliente
	 */
	public Optional<Cliente> recuperarCliente(Long id);
	
	/**
	 * Criar um cliente novo
	 * @param cliente objeto cliente
	 * @return o cliente criado;
	 */
	public Cliente gravarCliente(Cliente cliente);
	
	/**
	 * Exclui um cliente 
	 * @param id do cliente a ser excluído
	 */
	public void excluirCliente(Long id);

	/**
	 * Recupera um cliente por documento
	 * @param documento
	 * @return
	 */
	public Optional<Cliente> recuperarClientePorDocumento(String documento);
	
	/**
	 * Recupera o usuário por username
	 * @param username
	 * @return Optional<Cliente> 
	 */
	public Optional<Cliente> recuperarClientePorUsername(String username);
	

}
