package br.com.agmg.stonefinancserver.exception;

public class UsernameDocumentAlreadyExistsException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;

	public UsernameDocumentAlreadyExistsException(String message) {
		super(message);
	}

}
