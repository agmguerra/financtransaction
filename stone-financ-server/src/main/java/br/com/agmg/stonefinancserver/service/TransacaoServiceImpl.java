package br.com.agmg.stonefinancserver.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.CartaoSaldo;
import br.com.agmg.stonefinancserver.entity.Transacao;
import br.com.agmg.stonefinancserver.exception.TransactionNotProcessedException;
import br.com.agmg.stonefinancserver.repositorio.TransacaoRepository;

/**
 * @see br.com.agmg.stonefinancserver.service.TransacaoService
 */
@Service
public class TransacaoServiceImpl implements TransacaoService {
	
	@Autowired
	private TransacaoRepository repositorioTransacao;
	
	@Autowired
	private CartaoService cartaoService;
	
	@Autowired
	private CartaoSaldoService cartaoSaldoService;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.TransacaoService#recuperarTransacoes()
	 * 
	 */
	public List<Transacao> recuperarTransacoes() {	
		return repositorioTransacao.findAll();
	}
		
	/**
	 * @see br.com.agmg.stonefinancserver.service.TransacaoService#recuperarTransacao(Long)
	 */
	public List<Transacao> recuperarTransacoesPorUsername(String username) {
		return repositorioTransacao.findByCartaoClienteUsername(username);
	}

	/**
	 * @see br.com.agmg.stonefinancserver.service.TransacaoService#recuperarTransacao(Long)
	 */
	public Optional<Transacao> recuperarTransacao(Long id) {
		return repositorioTransacao.findById(id);
	}

	/**
	 * @see br.com.agmg.stonefinancserver.service.TransacaoService#processarTransacao(Transacao)
	 */
	public Transacao processarTransacao(Transacao transacao) {
	
		Transacao transacaoSalva = null;
		Cartao cartao = transacao.getCartao();
		
		
		if (cartaoSaldoService.isCartaoValido(cartao.getNumero(), cartao.getNome(), cartao.getDataDeExpiracao(), cartao.getCodigoSeguranca(), transacao.getValor(), cartao.getPassword())) {
			
			try {
				//Se o cartao é válido e possui saldo atualiza o saldo
				CartaoSaldo cartaoSaldo = new CartaoSaldo();
				BeanUtils.copyProperties(cartaoSaldo, cartao);
				cartaoSaldoService.atualizarSaldoCartao(transacao);
				
				//Persiste a transacao 1o se o cartão não existir persiste ele e depois a transacao
				Optional<Cartao> cartaoOpt = cartaoService.recuperarCartaoPorNumero(cartao.getNumero());
				Cartao cartaoTran = null;
				if (!cartaoOpt.isPresent()) {
					cartaoTran = cartaoService.gravarCartao(cartaoOpt.get());
				} else {
					cartaoTran = cartaoOpt.get();
				}
				
				transacao.getCartao().setId(cartaoTran.getId());
				transacaoSalva = repositorioTransacao.save(transacao);
			} catch (Exception e) {
				throw new TransactionNotProcessedException(messageSource.getMessage("erro.transacao.nao-processada.mensagem", null, request.getLocale()));
			} 
			
		}
		
		return transacaoSalva;
	}

}
