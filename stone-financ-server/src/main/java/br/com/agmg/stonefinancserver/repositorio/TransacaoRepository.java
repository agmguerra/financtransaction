package br.com.agmg.stonefinancserver.repositorio;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.Transacao;

/**
 * 
 * Representa o repositório de transações
 *
 */
@RepositoryRestResource(collectionResourceRel = "transacoes", exported = false)
public interface TransacaoRepository extends JpaRepository<Transacao, Long>{
	
	List<Transacao> findByCartao(Cartao cartao);
	List<Transacao> findByDataTransacaoBetween(Date startDate, Date endDate);
	List<Transacao> findByCartaoClienteUsername(String username);
	
}
