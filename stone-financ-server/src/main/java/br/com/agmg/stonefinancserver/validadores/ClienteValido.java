package br.com.agmg.stonefinancserver.validadores;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Target({ElementType.TYPE,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ClienteValidator.class)
public @interface ClienteValido {
	
	String message() default "{erro.cliente.invalido.mensagem}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}
