package br.com.agmg.stonefinancserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancserver.validadores.CartaoValido;
import io.swagger.annotations.ApiModel;

/**
 * Classe que representa o cartão
 * 
 * @author alexgmg
 *
 */
@ApiModel(description="Contém informações de um cartão de um cliente")
@Entity
@Table(name = "cartao")
@CartaoValido
public class Cartao implements Serializable {
		
	private static final long serialVersionUID = 1L;

	/**
	 * id do cartão
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "cartao_id", unique = true, nullable = false)
	private Long id;
	
	/**
	 * nome do dono no cartão
	 */
	@Column(name = "nome_cartao", nullable = false, length=60)
	private String nome;
	
	/**
	 * Número do cartão
	 */
	@Column(name = "numero_cartao", unique = true, nullable = false)
	private String numero;
	
	/**
	 * Data de expiração do cartão
	 */
	@Column(name = "dt_expiracao", nullable = false)
	private String dataDeExpiracao;
	
	/**
	 * Bandeira do cartão (VISA(0), MASTERCARD(1), etc)
	 */
	@Column(name = "bandeira_cartao", nullable = false)
	private BandeiraCartaoEnum bandeiraDoCartao;
		
	/**
	 * Tipo do cartão: 0 - chip, 1 - tarja magnética
	 */
	@Column(name = "tp_cartao", nullable = false)
	private TipoCartaoEnum tipoCartao;
	
	/**
	 * Se o tipo do cartão é CHIP então possuiPassword é true
	 */
	@Column(name = "possui_password", nullable = false)
	private Boolean possuiPassword;
	
	/**
	 * Código de segurança do cartao
	 */
	@Column(name = "cod_seg", nullable = true)
	@JsonIgnore
	private String codigoSeguranca;
	
	/**
	 * password do cartão
	 */
	@Transient
	private String password;

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name="cliente_id", nullable = false)
	private Cliente cliente;
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDataDeExpiracao() {
		return dataDeExpiracao;
	}

	public void setDataDeExpiracao(String dataDeExpiracao) {
		this.dataDeExpiracao = dataDeExpiracao;
	}

	public BandeiraCartaoEnum getBandeiraDoCartao() {
		return bandeiraDoCartao;
	}

	public void setBandeiraDoCartao(BandeiraCartaoEnum bandeiraDoCartao) {
		this.bandeiraDoCartao = bandeiraDoCartao;
	}

	public TipoCartaoEnum getTipoCartao() {
		return tipoCartao;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public Boolean getPossuiPassword() {
		return possuiPassword;
	}

	public void setPossuiPassword(Boolean possuiPassword) {
		this.possuiPassword = possuiPassword;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setTipoCartao(TipoCartaoEnum tipoCartao) {
		this.tipoCartao = tipoCartao;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	public String getCodigoSeguranca() {
		return codigoSeguranca;
	}
	
	@JsonProperty
	public void setCodigoSeguranca(String codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		
		final Cartao other = (Cartao) obj;
		if ( numero == null ) {
			if ( other.getNumero() != null ) {
				return false;
			}
		} else if( !numero.equals( other.getNumero() ) ) {
			return false;
		}
		
				
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( numero == null ) ? 0 : numero.hashCode() );
		return result;
	}

	
}
