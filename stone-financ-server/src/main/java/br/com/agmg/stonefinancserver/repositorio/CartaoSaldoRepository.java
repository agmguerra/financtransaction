package br.com.agmg.stonefinancserver.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.agmg.stonefinancserver.entity.CartaoSaldo;

/**
 * 
 * Classe que representa o repositório de Saldos dos cartões
 *
 */
@RepositoryRestResource(exported = false)
public interface CartaoSaldoRepository extends JpaRepository<CartaoSaldo, Long>{

	Optional<CartaoSaldo> findByNumero(String numero);
}
