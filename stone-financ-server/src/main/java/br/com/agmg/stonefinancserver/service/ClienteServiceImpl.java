package br.com.agmg.stonefinancserver.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.agmg.stonefinancserver.entity.Cliente;
import br.com.agmg.stonefinancserver.exception.GenericException;
import br.com.agmg.stonefinancserver.exception.UsernameDocumentAlreadyExistsException;
import br.com.agmg.stonefinancserver.repositorio.ClienteRepository;
import br.com.agmg.stonefinancserver.seguranca.criptografia.GenerateKeys;
import br.com.agmg.stonefinancserver.utils.StringUtils;

/**
 * @see br.com.agmg.stonefinancserver.service.ClienteService
 */
@Service
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteRepository repositorioClientes;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
		
	/**
	 * @see br.com.agmg.stonefinancserver.service.ClienteService#recuperarTodosClientes()
	 */
	@Override
	public List<Cliente> recuperarTodosClientes() {
		
		return repositorioClientes.findAll();
		
	}
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.ClienteService#recuperarClientePorDocumento(String)
	 */
	@Override
	public Optional<Cliente> recuperarClientePorDocumento(String documento) {
		return repositorioClientes.findByDocumento(documento);
	}
	
	
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.ClienteService#recuperarClientes(Integer, Integer)
	 */
	@Override
	public Page<Cliente> recuperarClientes(Integer pagina, Integer tamanhoPagina) {
		
		Pageable pageRequest = criarPageRequestOrdenadoPorDocumento(pagina, tamanhoPagina);		
		
		return repositorioClientes.findAll(pageRequest);
		
	}
	
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.ClienteService#recuperarCliente(Long)
	 */
	@Override
	public Optional<Cliente> recuperarCliente(Long id) {
		
		return  repositorioClientes.findById(id);
				
	}
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.ClienteService#gravarCliente(Cliente)
	 */
	@Override
	public Cliente gravarCliente(Cliente cliente) {
		
		cliente.setDocumento(StringUtils.retiraMascara(cliente.getDocumento()));
		
		try {
			
			GenerateKeys gk = new GenerateKeys(512);
			gk.createKeys();
			cliente.setPublicKey(gk.getPublicKey().getEncoded());
			cliente.setPrivateKey(gk.getPrivateKey().getEncoded());
						
			cliente.setPassword(bCryptPasswordEncoder.encode(cliente.getPassword()));
			
			Cliente cli = repositorioClientes.save(cliente);
					
			return cli;
		
		} catch (DataIntegrityViolationException die) {
			throw new UsernameDocumentAlreadyExistsException("erro.cliente.documento-ou-username-ja-cadastrados.mensagem");
		} catch (Exception e) {
			throw new GenericException(e.getMessage());
		} 

	}
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.ClienteService#excluirCliente(Long)
	 */
	@Override
	public void excluirCliente(Long id) {
		repositorioClientes.deleteById(id);
	}
	
	
	/**
	 * Criar page request
	 * @param pagina
	 * @param tamanhoPagina
	 */
	private Pageable criarPageRequestOrdenadoPorDocumento(Integer pagina, Integer tamanhoPagina) {
		return PageRequest.of(pagina, tamanhoPagina, new Sort(Sort.Direction.ASC, "documento"));
	}

	/**
	 * @see br.com.agmg.stonefinancserver.service.ClienteService#recuperarClientePorUsername(String)
	 */
	@Override
	public Optional<Cliente> recuperarClientePorUsername(String username) {
		
		return repositorioClientes.findByUsername(username);
	}
	
	

}
