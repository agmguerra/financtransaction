package br.com.agmg.stonefinancserver.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.agmg.stonefinancserver.enumeration.TipoDocumentoEnum;
import br.com.agmg.stonefinancserver.validadores.ClienteValido;
import io.swagger.annotations.ApiModel;

/**
 * Classe que representa o Cliente
 * 
 * @author alexgmg
 *
 */
@ApiModel(description="Contém informações do cliente")
@Entity
@Table(name = "cliente")
@ClienteValido
public class Cliente implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * id do cliente
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "cliente_id", unique = true, nullable = false)
	private Long id;

	/**
	 * nome do cliente
	 */
	@Column(name = "nome", nullable = false)
	private String nome;
	
	@JsonIgnore
	@Column(name = "username", unique = true, nullable = false)
	private String username;
	
	@JsonIgnore
	@Column(name = "password", nullable = false)
	private String password;	
	
	/**
	 * tipo de documento (0 - Cpf, 1 - Cnpj)
	 */
	@Column(name = "tp_documento", nullable = false)
	private TipoDocumentoEnum tipoDocumento;
	
	/**
	 * documento informado pelo cliente (cpf ou cnpj)
	 */
	@Column(name = "documento", unique = true, nullable = false)
	private String documento;
 	
	@Column(name = "private_key", columnDefinition = "BINARY(1000)")
	@JsonIgnore
	private byte[] privateKey;
	
	@Column(name = "public_key", columnDefinition = "BINARY(1000)")
	private byte[] publicKey;

	
	@OneToMany(mappedBy="cliente", fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Cartao> cartoes;

	
	public Cliente() {
		
	}
	
	public Cliente(Long id, String nome, TipoDocumentoEnum tipoDocumento, String documento) {
		this.id = id;
		this.nome = nome;
		this.tipoDocumento = tipoDocumento;
		this.documento = documento;
	}
	
	public Long getId() {
		return id;
	}

	
	public TipoDocumentoEnum getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoEnum tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cartao> getCartoes() {
		return cartoes;
	}

	public void setCartoes(List<Cartao> cartoes) {
		this.cartoes = cartoes;
	}
	
	public byte[] getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(byte[] privateKey) {
		this.privateKey = privateKey;
	}

	public byte[] getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(byte[] publicKey) {
		this.publicKey = publicKey;
	}
	
	
	@JsonIgnore
	public String getUsername() {
		return username;
	}

	@JsonProperty
	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		
		final Cliente other = (Cliente) obj;
		if ( documento == null ) {
			if ( other.getDocumento() != null ) {
				return false;
			}
		} else if( !documento.equals( other.getDocumento() ) ) {
			return false;
		}
		
				
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( documento == null ) ? 0 : documento.hashCode() );
		return result;
	}

}
