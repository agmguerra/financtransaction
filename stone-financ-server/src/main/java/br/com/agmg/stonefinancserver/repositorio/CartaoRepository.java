package br.com.agmg.stonefinancserver.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.agmg.stonefinancserver.entity.Cartao;

/**
 * 
 * Classe que representa o repositório de Cartões
 *
 */
@RepositoryRestResource(exported = false, collectionResourceRel = "cartoes")
public interface CartaoRepository extends JpaRepository<Cartao, Long>{

	Optional<Cartao> findByNumero(String numero);
}
