package br.com.agmg.stonefinancserver.teste;

import java.net.URI;
import java.net.URISyntaxException;

public class Testes {

	public static void main(String[] args) {

//		try {
//			GenerateKeys gk;
//			gk = new GenerateKeys(512);
//			gk.createKeys();
//			byte[] publicKey = gk.getPublicKey().getEncoded();
//			byte[] privateKey = gk.getPrivateKey().getEncoded();
//
//			String msg = "Alexandre Gomes Monteiro Guerra";
//			AsymmetricCryptography ac = new AsymmetricCryptography();
//			String encrypted_msg = ac.encryptText(msg, ac.getPublic(publicKey));
//			String decrypted_msg = ac.decryptText(encrypted_msg, ac.getPrivate(privateKey));
//			System.out.println("Original Message: " + msg +
//				"\nEncrypted Message: " + encrypted_msg
//				+ "\nDecrypted Message: " + decrypted_msg);
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		// Hash a password for the first time
//		String hashed = BCrypt.hashpw("1234", BCrypt.gensalt());
//		System.out.println(hashed);
//		// gensalt's log_rounds parameter determines the complexity
//		// the work factor is 2**log_rounds, and the default is 10
//		//String hashed = BCrypt.hashpw("1234", BCrypt.gensalt(12));
//
//		// Check that an unencrypted password matches one that has
//		// previously been hashed
//		if (BCrypt.checkpw("1234", hashed))
//			System.out.println("It matches");
//		else
//			System.out.println("It does not match");
		
		
		try {
			URI location = new URI("http://localhost:8080/api/transacoes/1");
			System.out.println(location.getRawQuery());
			System.out.println(location.getFragment());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
