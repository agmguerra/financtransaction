package br.com.agmg.stonefinancserver.resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.agmg.stonefinancserver.entity.Transacao;
import br.com.agmg.stonefinancserver.exception.NotFoundException;
import br.com.agmg.stonefinancserver.service.TransacaoService;

@RestController
public class TransacaoResource {
	
	@Autowired
	private TransacaoService transacaoService;
	
	@Autowired
	private MessageSource messageSource;

	@Autowired
	private HttpServletRequest request;

	@GetMapping("/api/transacoes")
	public ResponseEntity<Resources<Resource<Transacao>>> recuperarTodasTransacoes(@PathParam("username") String username) {
				
		List<Transacao> transacoes = null;
		
		if (username == null || username.isEmpty()) {
		
			transacoes = transacaoService.recuperarTransacoes();
			
		} else {
			
			transacoes = transacaoService.recuperarTransacoesPorUsername(username);
		}

		List<Resource<Transacao>> resourcesList = transacoes.stream().map(transacao -> transacaoToResource(transacao)).collect(Collectors.toList());
		
		Resources<Resource<Transacao>> resources = new Resources<Resource<Transacao>>(resourcesList);

		return ResponseEntity.ok(resources);
		
	}
	
	@GetMapping("/api/transacoes/{id}")
	public ResponseEntity<Resource<Transacao>> recuperarTransacao(@PathVariable Long id) {
		Optional<Transacao> transacao = transacaoService.recuperarTransacao(id); 
		if (!transacao.isPresent()) {
			throw new NotFoundException(
					messageSource.getMessage("erro.transacao.nao-encotrada.mensagem", new Object[]{id}, request.getLocale()));
		}
		
		Resource<Transacao> resource = new Resource<Transacao>(transacao.get());
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).recuperarTodasTransacoes(null));
		
		resource.add(linkTo.withRel("all-transacoes"));
		
		return ResponseEntity.ok(resource);	
	}

	@PostMapping("/api/transacoes")
	public ResponseEntity<Object> processarTransacao(@Valid @RequestBody Transacao transacao) {
		
		Transacao transacaoGravada = transacaoService.processarTransacao(transacao);
		
		StatusResponse resp = new StatusResponse(HttpStatus.CREATED, messageSource.getMessage("transacao.valida.mensagem", 
				new Object[]{transacaoGravada.getId()}, request.getLocale()), null);
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(transacaoGravada.getId()).toUri();

		ResponseEntity<Object> ret = ResponseEntity.created(location).body(resp);

	
		return ret;
	}

	private Resource<Transacao> transacaoToResource(Transacao transacao) {
		
		//Colocar o link para recuperar todos os usuários
				//retrieveAllUsers seguindo o padrão HATEOAS
		Resource<Transacao> resource = new Resource<Transacao>(transacao);
								 
		return resource;
		 
	}
	
}
