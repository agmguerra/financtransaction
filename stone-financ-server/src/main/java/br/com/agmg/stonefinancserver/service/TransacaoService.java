package br.com.agmg.stonefinancserver.service;

import java.util.List;
import java.util.Optional;

import br.com.agmg.stonefinancserver.entity.Transacao;

/**
 * Interface para as operações associadas a transação
 *
 */
public interface TransacaoService {
	
	/**
	 * Recupera todas as transações
	 * @return lista com todas as transações
	 */
	public List<Transacao> recuperarTransacoes();
	
	/**
	 * Recupera todas as transações de um username
	 * @return lista com todas as transações
	 */
	public List<Transacao> recuperarTransacoesPorUsername(String username);

	
	/**
	 * Recupera uma transação por Id
	 * @param id id da transação
	 * @return transação ou nulo
	 */
	public Optional<Transacao> recuperarTransacao(Long id);
	
	/**
	 * Processa a transação
	 * @param transacao objeto do tipo transação
	 * @return transação processada
	 */
	public Transacao processarTransacao(Transacao transacao);

}
