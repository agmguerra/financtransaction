package br.com.agmg.stonefinancserver.validadores;

import java.math.BigDecimal;
import java.math.MathContext;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.agmg.stonefinancserver.entity.Transacao;
import br.com.agmg.stonefinancserver.enumeration.TipoTransacaoEnum;
/**
 * Classe validadora dos atributos da transação
 *
 */
public class TransacaoValidator implements ConstraintValidator<TransacaoValido, Transacao>{
	
	
	private static final BigDecimal VALOR_MIN_TRANSACAO = new BigDecimal(0.10, new MathContext(2));
	
	/**
	 * valida o documento
	 */
	@Override
	public boolean isValid(Transacao transacao, ConstraintValidatorContext ctx) {
		
		boolean ret = true;
		if (transacao == null) {
			ctx.disableDefaultConstraintViolation();
	        ctx.buildConstraintViolationWithTemplate("{erro.transaction.invalida.mensagem}")
	           .addConstraintViolation();

			ret = false;
		} else {
			if (transacao.getValor() == null || transacao.getValor().compareTo(VALOR_MIN_TRANSACAO) == -1) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.transacao.valor.invalido.mensagem}")
		           .addConstraintViolation();
				ret = false;
			} 
			if (transacao.getTipoTransacao() == null || (transacao.getTipoTransacao() != TipoTransacaoEnum.CREDITO
					&& transacao.getTipoTransacao() != TipoTransacaoEnum.DEBITO)) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.transacao.tipo-transacao.invalido.mensagem}")
		           .addConstraintViolation();
				ret = false;
			} else if (transacao.getNumeroParcelas() != null) {
				if (transacao.getNumeroParcelas() < 0) {
					ctx.disableDefaultConstraintViolation();
			        ctx.buildConstraintViolationWithTemplate("{erro.transacao.numero-parcelas-tipo.mensagem}")
			           .addConstraintViolation();
					ret = false;													
				} else if (transacao.getTipoTransacao() == TipoTransacaoEnum.DEBITO && 
						transacao.getNumeroParcelas() > 0) {
					ctx.disableDefaultConstraintViolation();
			        ctx.buildConstraintViolationWithTemplate("{erro.transacao.numero-parcelas-tipo.mensagem}")
			           .addConstraintViolation();
					ret = false;									
				} 
			}
		}
			
		return ret;
	}
	
}
