package br.com.agmg.stonefinancserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.Cliente;
import br.com.agmg.stonefinancserver.entity.Transacao;

/**
 * Classe de configuração que permite expor os id no retorno dos serviços quando usando
 * HATEOAS
 * 
 *
 */
@Configuration
public class RestConfiguration extends RepositoryRestConfigurerAdapter {

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Cliente.class, Cartao.class, Transacao.class);

	}
	
}
