package br.com.agmg.stonefinancserver.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.agmg.stonefinancserver.enumeration.TipoTransacaoEnum;
import br.com.agmg.stonefinancserver.validadores.CartaoValido;
import br.com.agmg.stonefinancserver.validadores.TransacaoValido;
import io.swagger.annotations.ApiModel;

/**
 * Classe que representa um transacao com um cartão
 *
 */
@ApiModel(description="Contém informações de transacao")
@Entity
@Table(name = "transacao")
@TransacaoValido
public class Transacao implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id da transacao
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "transacao_id", unique = true, nullable = false)
	private Long id;

	/**
	 * Data da transação
	 */
	@Column(name = "data_transacao", nullable = false, insertable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date dataTransacao;
	
	/**
	 * Valor da transação
	 */
	@Column(name = "valor_transacao", nullable = false)
	private BigDecimal valor;
	
	/**
	 * Tipo de transação (DEBITO - 0 CREDITO - 1)
	 */
	@Column(name = "tipo_transacao", nullable = false)
	private TipoTransacaoEnum tipoTransacao;
	
	/**
	 * Número de parcelas da transação se o tipo for CRÉDITO
	 */
	@Column(name = "num_parcelas", nullable = true, columnDefinition="INTEGER(10) DEFAULT 0")
	private Integer numeroParcelas;
	
	/**
	 * Cartao associado a transação
	 */
	@ManyToOne
	@JoinColumn(name="cartao_id", nullable = false, updatable = false)
	@CartaoValido
	private Cartao cartao;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataTransacao() {
		return dataTransacao;
	}

	public void setDataTransacao(Date dataTransacao) {
		this.dataTransacao = dataTransacao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public TipoTransacaoEnum getTipoTransacao() {
		return tipoTransacao;
	}

	public void setTipoTransacao(TipoTransacaoEnum tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}

	public Cartao getCartao() {
		return cartao;
	}

	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}
	
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		
		final Transacao other = (Transacao) obj;
		if ( dataTransacao == null ) {
			if ( other.getDataTransacao() != null ) {
				return false;
			}
		} else if( !dataTransacao.equals( other.getDataTransacao() ) ) {
			return false;
		}
		
		if (cartao == null) {
			if (other.getCartao() != null) {
				return false;
			}
		} else if (!cartao.equals(other.getCartao())) {
			return false;
		}
		
		if (valor == null) {
			if (other.getValor() != null) {
				return false;
			}
		} else if (!valor.equals(other.getValor())) {
			return false;
		}
		
				
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( dataTransacao == null ) ? 0 : dataTransacao.hashCode() );
		result = prime * result + ( ( cartao == null ) ? 0 : cartao.hashCode() );
		return result;
	}

	
}
