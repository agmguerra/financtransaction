package br.com.agmg.stonefinancserver.resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.collect.Lists;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.Cliente;
import br.com.agmg.stonefinancserver.exception.NotFoundException;
import br.com.agmg.stonefinancserver.service.ClienteService;

/**
 * Classe que expõe a api rest para cliente
 *
 */
@RestController
public class ClienteResource  {

	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * Recupera todos os clientes
	 * @param documento
	 * @return ResponseEntity<Resources<Resource<Cliente>>>
	 */
	@GetMapping("/api/clientes")
	public ResponseEntity<Resources<Resource<Cliente>>> recuperarClientes(@PathParam("documento") String documento) {
		
		List<Cliente> clientes = null;
		
		if (documento == null || documento.isEmpty()) {		
			clientes = clienteService.recuperarTodosClientes();
		} else {
			Optional<Cliente> cliente = clienteService.recuperarClientePorDocumento(documento);
			if (cliente.isPresent()) {
				clientes = Lists.newArrayList(cliente.get());
			} else {
				throw new NotFoundException(messageSource.getMessage("erro.cliente.nao-encontrado.mensagem", 
						new String[]{"documento=" + documento}, request.getLocale()));				
			}
		}
				
		List<Resource<Cliente>> resourcesList = clientes.stream().map(cliente -> new Resource<Cliente>(cliente)).collect(Collectors.toList());
		
		Resources<Resource<Cliente>> resources = new Resources<Resource<Cliente>>(resourcesList);

		
		return ResponseEntity.ok(resources);
	}


	
	@GetMapping("/api/clientes/{id}")
	public ResponseEntity<Resource<Cliente>> recuperarCliente(@PathVariable Long id) {
		Optional<Cliente> cliente = clienteService.recuperarCliente(id); 
		if (!cliente.isPresent()) {
			throw new NotFoundException(messageSource.getMessage("erro.cliente.nao-encontrado.mensagem", 
					new String[]{"id=" + id}, request.getLocale()));
		}
		
		//Colocar o link para recuperar todos os usuários e para os cartões do usuário
		//seguindo o padrão HATEOAS
		Resource<Cliente> resource = new Resource<Cliente>(cliente.get());
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).recuperarClientes(null));
		ControllerLinkBuilder linkCartaoTo = linkTo(methodOn(this.getClass()).recuperarTodosCartoesByClienteId(id));
		
		resource.add(linkCartaoTo.withRel("cartoes"));
		resource.add(linkTo.withRel("all-clientes"));
		
		return ResponseEntity.ok(resource);	
	}
	

	
	@GetMapping("/api/clientes/{id}/cartoes")
	public ResponseEntity<Resources<Resource<Cartao>>> recuperarTodosCartoesByClienteId(@PathVariable Long id) {
		Optional<Cliente> cliente = clienteService.recuperarCliente(id); 
		if (!cliente.isPresent()) {
			throw new NotFoundException(messageSource.getMessage("erro.cliente.nao-encontrado.mensagem", 
					new String[]{"id=" + id}, request.getLocale()));
		}
		
		List<Cartao> cartoes = cliente.get().getCartoes();
		
		
		List<Resource<Cartao>> resourcesList = cartoes.stream().map(cartao -> new Resource<Cartao>(cartao)).collect(Collectors.toList());

		//Colocar o link para o usuário dono do cartão
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).recuperarCliente(id));
		
		Resources<Resource<Cartao>> resources = new Resources<Resource<Cartao>>(resourcesList);
		resources.add(linkTo.withRel("cliente"));
		
		return ResponseEntity.ok(resources);	
	}

	
	
	@PostMapping("/api/clientes")
	public ResponseEntity<Object> gravarCliente(@Valid @RequestBody Cliente cliente) {
		
		Cliente clienteGravado = clienteService.gravarCliente(cliente);
				
		URI location = ServletUriComponentsBuilder
							.fromCurrentRequest()
							.path("/{id}")
							.buildAndExpand(clienteGravado.getId()).toUri();
		
		ResponseEntity<Object> ret = ResponseEntity.created(location).build();

		return ret;
			
			
	}

	@DeleteMapping("/api/clientes/{id}")
	public void excluirCliente(@PathVariable Long id) {
		clienteService.excluirCliente(id);;
	}

	
	
}
