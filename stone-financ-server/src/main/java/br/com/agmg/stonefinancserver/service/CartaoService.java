package br.com.agmg.stonefinancserver.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import br.com.agmg.stonefinancserver.entity.Cartao;

/**
 * Interface de serviços de Cartão
 *
 */
public interface CartaoService {

	/**
	 * Recuperar todos os cartoes
	 * 
	 * @return List<Cartao> - lists de cartoes
	 */
	public List<Cartao> recuperarTodosCartoes();

	/**
	 * Recuperar os cartoes por pagina por id
	 * @param pagina número da página que se quer recuperar
	 * @param tamanhoPagina número de objetos Cartao da página que ser quer recuperar
	 * @return
	 */
	public Page<Cartao> recuperarCartoes(Integer pagina, Integer tamanhoPagina);
	
	/**
	 * Recuperar todos os cartoes
	 * 
	 * @return List<Cartao> - lists de cartoes
	 */
	public Optional<Cartao> recuperarCartao(Long id);

	/**
	 * Recuperar o cartão por numero
	 * @param numero
	 * @return Optional<Cartao> cartão recuperado
	 */
	public Optional<Cartao> recuperarCartaoPorNumero(String numero);
	
	
	/**
	 * Criar um cartao novo
	 * @param cartao objeto cartao
	 * @return o cliente criado;
	 */
	public Cartao gravarCartao(Cartao cartao);
	
	/**
	 * Exclui um cartao 
	 * @param id do cartao a ser excluído
	 */
	public void excluirCartao(Long id);
	
}