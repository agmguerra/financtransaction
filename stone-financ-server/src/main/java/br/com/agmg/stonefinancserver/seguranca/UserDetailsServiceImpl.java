package br.com.agmg.stonefinancserver.seguranca;

import static java.util.Collections.emptyList;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.agmg.stonefinancserver.entity.Cliente;
import br.com.agmg.stonefinancserver.service.ClienteService;

/**
 * 
 * Classe responsável por validar a existência do cliente na base para autenticação
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private ClienteService clienteService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Cliente> cliente = clienteService.recuperarClientePorUsername(username);
        if (!cliente.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        return new User(cliente.get().getUsername(), cliente.get().getPassword(), emptyList());
    }

}
