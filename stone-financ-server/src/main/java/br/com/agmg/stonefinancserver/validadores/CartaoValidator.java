package br.com.agmg.stonefinancserver.validadores;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
/**
 * Classe validadora dos atributos da transação
 *
 */
public class CartaoValidator implements ConstraintValidator<CartaoValido, Cartao>{
	
	private static final int NOME_CARTAO_MAX_SIZE = 60;
	
	/**
	 * valida o documento
	 */
	@Override
	public boolean isValid(Cartao cartao, ConstraintValidatorContext ctx) {
		
		boolean ret = true;
		
		if (cartao == null) {
			ctx.disableDefaultConstraintViolation();
	        ctx.buildConstraintViolationWithTemplate("{erro.cartao.invalido.mensagem}")
	        	.addConstraintViolation();		
			ret = false;
		} else {
			if (cartao.getTipoCartao() == null || (cartao.getTipoCartao() != TipoCartaoEnum.CHIP &&
				cartao.getTipoCartao() != TipoCartaoEnum.TARJA) ) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cartao.tipo-cartao.invalido.mensagem}")
		        	.addConstraintViolation();		
				ret = false;
			} else if (cartao.getPossuiPassword() && cartao.getTipoCartao() == TipoCartaoEnum.TARJA) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cartao.possui-password.invalido.mensagem}")
		        	.addConstraintViolation();		
				ret = false;			
			} 
			
			if (cartao.getBandeiraDoCartao() == null || (cartao.getBandeiraDoCartao() != BandeiraCartaoEnum.MASTERCARD &&
				cartao.getBandeiraDoCartao() != BandeiraCartaoEnum.VISA)) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cartao.bandeira-cartao.invalido.mensagem}")
		        	.addConstraintViolation();		
				ret = false;						
			} 
			if (cartao.getNome() == null || cartao.getNome().isEmpty() || cartao.getNome().length() > NOME_CARTAO_MAX_SIZE) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cartao.nome.invalido.mensagem}")
		        	.addConstraintViolation();		
				ret = false;									
			}
			
			DateTimeFormatter formatter = null;
			YearMonth dtAnoMesExp = null;
			YearMonth dtAnoMesNow = YearMonth.now();
			if (cartao.getDataDeExpiracao().contains("/")) {
				formatter = DateTimeFormatter.ofPattern ( "MM/yy" );
			} else {
				formatter = DateTimeFormatter.ofPattern ( "MMyy" );
			}
			dtAnoMesExp = YearMonth.parse ( cartao.getDataDeExpiracao() , formatter);
			
			if (cartao.getDataDeExpiracao() == null || cartao.getDataDeExpiracao().isEmpty() || dtAnoMesExp.isBefore(dtAnoMesNow)) {
				ctx.disableDefaultConstraintViolation();
		        ctx.buildConstraintViolationWithTemplate("{erro.cartao.data-expiracao.invalido.mensagem}")
		        	.addConstraintViolation();		
				ret = false;
			}
		}
			
		
		return ret;
	}
	
}
