package br.com.agmg.stonefinancserver.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.repositorio.CartaoRepository;

/**
 * @see br.com.agmg.stonefinancserver.service.CartaoServce
 *
 */
@Service
public class CartaoServiceImpl implements CartaoService {
	
	@Autowired
	private CartaoRepository cartaoRepository;
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoService#recuperarTodosCartoes()
	 */
	@Override
	public List<Cartao> recuperarTodosCartoes() {
		
		return cartaoRepository.findAll();
		
	}
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoService#recuperarCartoes(Integer, Integer)
	 */
	@Override
	public Page<Cartao> recuperarCartoes(Integer pagina, Integer tamanhoPagina) {
		
		Pageable pageRequest = criarPageRequestOrdenadoPorId(pagina, tamanhoPagina);		
		
		return cartaoRepository.findAll(pageRequest);
		
	}
	
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoService#recuperarCartao(Long)
	 */
	@Override
	public Optional<Cartao> recuperarCartao(Long id) {
		
		return  cartaoRepository.findById(id);
				
	}
	
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoService#recuperarCartaoPorNumero(String)
	 */
	@Override
	public Optional<Cartao> recuperarCartaoPorNumero(String numero) {
		
		return  cartaoRepository.findByNumero(numero);
				
	}

	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoService#gravarCartao(Cartao)
	 */
	@Override
	public Cartao gravarCartao(Cartao cartao) {
		
		Cartao card = cartaoRepository.save(cartao);
		return card;
	}
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoService#excluirCartao(Long)
	 */
	@Override
	public void excluirCartao(Long id) {
		cartaoRepository.deleteById(id);
	}
	
	
	/**
	 * Criar um page request
	 * @param número da página
	 * @param tamanho da página
	 * @return Pageable
	 */
	private Pageable criarPageRequestOrdenadoPorId(Integer pagina, Integer tamanhoPagina) {
		return PageRequest.of(pagina, tamanhoPagina, new Sort(Sort.Direction.ASC, "id"));
	}

}
