package br.com.agmg.stonefinancserver.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Representa os possíveis valores de tipo de documento
 *
 */
public enum TipoDocumentoEnum {
		
	CPF(0), CNPJ(1);

	Integer valor;
	
	TipoDocumentoEnum(int valor) {
		this.valor = valor;
	}

	public Integer getValue() {
		return valor;
	}
	
	/**
	 * Cria o Enum a partir de um Json. Se o valor
	 * não estiver no domínio retorna null
	 * @param param valor informado do  tipo do documento
	 * @return Enum referente ao tipo informada
	 */
	@JsonCreator
    public static TipoDocumentoEnum create (String param) {
		TipoDocumentoEnum ret = null;
        if(param != null) {
            for(TipoDocumentoEnum tipo : values()) {
                if(param.equals(tipo.getValue()) || param.equals(tipo.name())) {
                    ret = tipo;
                }
            }
        }
        return ret;
    }

}
