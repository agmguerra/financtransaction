package br.com.agmg.stonefinancserver.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.agmg.stonefinancserver.entity.Cliente;


/**
 * Representa o repository para cadastramento de clientes
 * 
 * @author alexgmg
 *
 */
@RepositoryRestResource(exported = false)
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	Optional<Cliente> findByDocumento(String documento);
	
	Optional<Cliente> findByUsername(String username);

}
