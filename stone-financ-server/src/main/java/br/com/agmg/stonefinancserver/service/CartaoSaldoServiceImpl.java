package br.com.agmg.stonefinancserver.service;

import java.math.BigDecimal;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import br.com.agmg.stonefinancserver.entity.CartaoSaldo;
import br.com.agmg.stonefinancserver.entity.Transacao;
import br.com.agmg.stonefinancserver.exception.TransactionNotApprovedException;
import br.com.agmg.stonefinancserver.repositorio.CartaoSaldoRepository;

/**
 * @see br.com.agmg.stonefinancserver.service.CartaoSaldoService
 *
 */
@Service
public class CartaoSaldoServiceImpl implements CartaoSaldoService {
	
	@Autowired
	private CartaoSaldoRepository cartaoSaldoRepository;
		
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private HttpServletRequest request;
	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoSaldoService#isCartaoValido(CartaoBuscaDto)
	 */
	@Override
	public Boolean isCartaoValido(String numero, String nome, String dataExpiracao, 
			String codigoSegurancao, BigDecimal valor, String password) {

		Optional<CartaoSaldo> cartaoOpt = cartaoSaldoRepository.findByNumero(numero);
		
		if (cartaoOpt.isPresent()) {
			CartaoSaldo cartaoLido = cartaoOpt.get();
			
			
			if (!cartaoLido.getDataDeExpiracao().equals(dataExpiracao)) {
				throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.data-expiracao.mensagem", null, request.getLocale()));
			}
	
			YearMonth dtAnoMesNow = YearMonth.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern ( "MM/yy" );
			YearMonth dtAnoMesExp = YearMonth.parse ( dataExpiracao , formatter);
			
			if (dtAnoMesExp.isBefore(dtAnoMesNow)) {
				throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.data-expiracao.mensagem", null, request.getLocale()));
			}
			
			
			if (!cartaoLido.getNome().equals(nome)) {
				throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.nome-invalido.mensagem", null, request.getLocale()));
			}
			
			if (cartaoLido.getCodigoSeguranca() == null || !cartaoLido.getCodigoSeguranca().equals(codigoSegurancao)) {
				throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.codigo-segurancao.mensagem", null, request.getLocale()));
			}
			
			if (cartaoLido.getSaldo().compareTo(valor) == -1) {
				throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.valor.mensagem", null, request.getLocale()));
			}
					
			if (cartaoLido.getPossuiPassword() && !isPasswordValid(password, cartaoLido.getPassword())) {
				throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.password.mensagem", null, request.getLocale()));
			}
			
		} else {
			throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.nao-encontrado.mensagem", new String[]{numero}, request.getLocale()));
		}
	
		return true;
	}

	
	/**
	 * @see br.com.agmg.stonefinancserver.service.CartaoSaldoService#atualizarSaldoCartao(CartaoSaldo)
	 */
	@Override
	public CartaoSaldo atualizarSaldoCartao(Transacao transacao) {
		
		CartaoSaldo cartaoSaldo = null;
		Optional<CartaoSaldo> crtOpt = cartaoSaldoRepository.findByNumero(transacao.getCartao().getNumero());
		if (crtOpt.isPresent()) {
			CartaoSaldo crtSaldo = crtOpt.get();
			crtSaldo.setSaldo(crtSaldo.getSaldo().subtract(transacao.getValor()));
			cartaoSaldo = cartaoSaldoRepository.save(crtSaldo);	
			
		} else {
			throw new TransactionNotApprovedException(messageSource.getMessage("erro.cartao.nao-encontrado.mensagem", new String[]{transacao.getCartao().getNumero()}, request.getLocale()));
		}
		
		return cartaoSaldo;
	}
	
	
	private boolean isPasswordValid(String passwordRecebida, String passwordLida) {
		
		if (BCrypt.checkpw(passwordRecebida, passwordLida)) {
			return true;
		} else {
			return false;
		}

	}
	

}
