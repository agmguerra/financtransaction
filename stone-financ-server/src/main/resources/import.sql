INSERT INTO CLIENTE(CLIENTE_ID, NOME, TP_DOCUMENTO, DOCUMENTO, USERNAME, PASSWORD) VALUES (1, 'Alexandre Guerra', 0, '98433683713', 'teste', '$2a$10$109cPYUgD3e5PhwaK1vftelpHV8IxfMQMIhkg15rKtn9Y6ixT4LlC');
INSERT INTO CLIENTE(CLIENTE_ID, NOME, TP_DOCUMENTO, DOCUMENTO, USERNAME, PASSWORD) VALUES (2, 'Manuel da Silva', 0, '24386858245', 'teste2', '$2a$10$109cPYUgD3e5PhwaK1vftelpHV8IxfMQMIhkg15rKtn9Y6ixT4LlC');

INSERT INTO CARTAO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, CLIENTE_ID, COD_SEG) VALUES (1, 'João da Silva', '1234567890123456', '12/18', 0, 0, 0, 1, '111');
INSERT INTO CARTAO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, CLIENTE_ID, COD_SEG) VALUES (2, 'Manuel da Silva', '1234567890123400', '12/19', 0, 0, 0, 2, '111');


INSERT INTO TRANSACAO ( TRANSACAO_ID, DATA_TRANSACAO, NUM_PARCELAS, TIPO_TRANSACAO, VALOR_TRANSACAO, CARTAO_ID ) VALUES (1, '2018-12-31', 0, 0, 100, 1)
INSERT INTO TRANSACAO ( TRANSACAO_ID, DATA_TRANSACAO, NUM_PARCELAS, TIPO_TRANSACAO, VALOR_TRANSACAO, CARTAO_ID ) VALUES (2, '2018-12-31', 0, 0, 100, 2)

INSERT INTO CARTAO_SALDO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, PASSWORD, SALDO, COD_SEG) VALUES (1, 'João da Silva', '1234567890123456', '12/18', 0, 0, 0, '$2a$10$yA6EckI6YuJL91FzM0kUU.2qKtqp32l5fMGSk4jw8mRiMlL9drOhq', 1000, '111');
INSERT INTO CARTAO_SALDO(CARTAO_ID, NOME_CARTAO, NUMERO_CARTAO, DT_EXPIRACAO, BANDEIRA_CARTAO, TP_CARTAO, POSSUI_PASSWORD, PASSWORD, SALDO, COD_SEG) VALUES (2, 'Manuel da Silva', '1234567890123400', '12/19', 0, 0, 0, '$2a$10$yA6EckI6YuJL91FzM0kUU.2qKtqp32l5fMGSk4jw8mRiMlL9drOhq', 1000, '111');
