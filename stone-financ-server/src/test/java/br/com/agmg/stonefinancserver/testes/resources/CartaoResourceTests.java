package br.com.agmg.stonefinancserver.testes.resources;


import static br.com.agmg.stonefinancserver.utils.StringUtils.asJsonString;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Optional;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancserver.resource.CartaoResource;
import br.com.agmg.stonefinancserver.resource.ClienteResource;
import br.com.agmg.stonefinancserver.service.CartaoService;

/**
 * 
 * Teste unitário da classe de resource CartaoResource
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CartaoResource.class)
public class CartaoResourceTests {
	
	@Autowired
    private MockMvc mvc;
 
	@MockBean
	private ClienteResource clienteResource;
	
    @MockBean
    private CartaoService service;
    
    Cartao crt = null;
    Cartao crtSemId = null;
    
	@Before
	public void preparaTestes() {
		
		crt = new Cartao();
		crt.setId(new Long(1));
		crt.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crt.setCodigoSeguranca("111");
		crt.setDataDeExpiracao("12/18");
		crt.setNome("João da Silva");
		crt.setNumero("1234567890123456");
		crt.setPossuiPassword(true);
		crt.setTipoCartao(TipoCartaoEnum.CHIP);
		
		crtSemId = new Cartao();
		crtSemId.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crtSemId.setCodigoSeguranca("111");
		crtSemId.setDataDeExpiracao("12/18");
		crtSemId.setNome("João da Silva");
		crtSemId.setNumero("1234567890123456");
		crtSemId.setPossuiPassword(true);
		crtSemId.setTipoCartao(TipoCartaoEnum.CHIP);

    	
    	given(service.recuperarTodosCartoes()).willReturn(Arrays.asList(crt));
    	given(service.recuperarCartao(new Long(1))).willReturn(Optional.of(crt));
    	given(service.gravarCartao(crtSemId)).willReturn(crt);

	}

    
    @Test
    public void recuperarTodosCartoesTeste() throws Exception {
    	    	    	
    	mvc.perform(get("/api/cartoes")
    		      .contentType(MediaType.APPLICATION_JSON))
    		      .andExpect(status().isOk())
    		      .andExpect(jsonPath("$['_embedded']['cartaos']", hasSize(1)))
    		      .andExpect(jsonPath("$['_embedded']['cartaos'][0].numero", Is.is(crt.getNumero())));
    	
    }
 
    @Test
    public void recuperarCartaoPorIdTeste() throws Exception {
    	    	
    	mvc.perform(get("/api/cartoes/1")
  		      .contentType(MediaType.APPLICATION_JSON))
  		      .andExpect(status().isOk())
  		      .andExpect(jsonPath("$.['numero']", Is.is(crt.getNumero())));

    	
    }
    
     
	@Test
	public void gravarCartao() throws Exception {

		
		mvc.perform(post("/api/cartoes")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(crtSemId)))
				.andExpect(status().isCreated());

	}

}
