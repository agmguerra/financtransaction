package br.com.agmg.stonefinancserver.testes.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.agmg.stonefinancserver.StoneFinancServerApplication;
import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.CartaoSaldo;
import br.com.agmg.stonefinancserver.entity.Transacao;
import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoTransacaoEnum;
import br.com.agmg.stonefinancserver.exception.TransactionNotApprovedException;
import br.com.agmg.stonefinancserver.repositorio.CartaoSaldoRepository;
import br.com.agmg.stonefinancserver.service.CartaoSaldoService;
import br.com.agmg.stonefinancserver.service.CartaoSaldoServiceImpl;

/**
 * Classe para teste de CartaoSaldoServicos
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoneFinancServerApplication.class)
public class CartaoSaldoServiceTests {

	@TestConfiguration
    static class CartaoSaldoServiceImplTestContextConfiguration {
  
        @Bean
        public CartaoSaldoService cartaoSaldoService() {
            return new CartaoSaldoServiceImpl();
        }
    }
 
    @Autowired
    private CartaoSaldoService cartaoSaldoService;
    
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private HttpServletRequest request;
 
    @MockBean
    private CartaoSaldoRepository cartaoSaldoRepository;
 
    
    private Cartao crtSemId = null;
    private CartaoSaldo crtSaldo = null;
    private CartaoSaldo crtSaldoAtu = null;
    private Transacao tr = null;
    
    private static final String PASSWORD_VALIDA = "1234";
    private static final String PASSWORD_CRYPTO = BCrypt.hashpw(PASSWORD_VALIDA, BCrypt.gensalt());
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
			
	@Before
	public void preparaTestes() {
		
		crtSemId = new Cartao();
		crtSemId.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crtSemId.setCodigoSeguranca("111");
		crtSemId.setDataDeExpiracao("12/18");
		crtSemId.setNome("Joao da Silva");
		crtSemId.setNumero("1234567890123456");
		crtSemId.setPassword("1234");
		crtSemId.setPossuiPassword(true);
		crtSemId.setTipoCartao(TipoCartaoEnum.CHIP);
		tr = new Transacao();
		tr.setTipoTransacao(TipoTransacaoEnum.CREDITO);
		tr.setNumeroParcelas(0);
		tr.setValor(new BigDecimal(100));
		tr.setCartao(crtSemId);
			
		crtSaldo = new CartaoSaldo();
		crtSaldo.setId(1L);
		crtSaldo.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crtSaldo.setCodigoSeguranca("111");
		crtSaldo.setDataDeExpiracao("12/18");
		crtSaldo.setNome("Joao da Silva");
		crtSaldo.setNumero("1234567890123456");
		crtSaldo.setPassword(PASSWORD_CRYPTO);
		crtSaldo.setPossuiPassword(true);
		crtSaldo.setSaldo(new BigDecimal(1000));
		crtSaldo.setTipoCartao(TipoCartaoEnum.CHIP);

		crtSaldoAtu = new CartaoSaldo();
		crtSaldoAtu.setId(1L);
		crtSaldoAtu.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crtSaldoAtu.setCodigoSeguranca("111");
		crtSaldoAtu.setDataDeExpiracao("12/18");
		crtSaldoAtu.setNome("Joao da Silva");
		crtSaldoAtu.setNumero("1234567890123456");
		crtSaldoAtu.setPassword(PASSWORD_CRYPTO);
		crtSaldoAtu.setPossuiPassword(true);
		crtSaldoAtu.setSaldo(new BigDecimal(900));
		crtSaldoAtu.setTipoCartao(TipoCartaoEnum.CHIP);



				
		Mockito.when(cartaoSaldoRepository.findByNumero(crtSemId.getNumero()))
		  .thenReturn(Optional.of(crtSaldo));
		
		Mockito.when(cartaoSaldoRepository.save(crtSaldo)).thenReturn(crtSaldoAtu);
				
	}
	
	@Test
	public void testeIsCartaoValidoOk() {
				
		Boolean isValid = cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), crtSemId.getNome(), crtSemId.getDataDeExpiracao(), 
														    crtSemId.getCodigoSeguranca(), tr.getValor(), crtSemId.getPassword());	
		assertTrue(isValid);
					
	}
		
	@Test
	public void testeIsCartaoPasswordInvalida() {
				
		thrown.expect(TransactionNotApprovedException.class);
        thrown.expectMessage(messageSource.getMessage("erro.cartao.password.mensagem", null, request.getLocale()));
		
		cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), crtSemId.getNome(), crtSemId.getDataDeExpiracao(), 
				                          crtSemId.getCodigoSeguranca(), tr.getValor(), "2345");					
	}
	
	@Test
	public void testeIsCartaoDataExpiracaoDiferenteInvalida() {
				
		thrown.expect(TransactionNotApprovedException.class);
        thrown.expectMessage(messageSource.getMessage("erro.cartao.data-expiracao.mensagem", null, request.getLocale()));
		
		cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), crtSemId.getNome(), "11/18", crtSemId.getCodigoSeguranca(), 
				                          tr.getValor(), crtSemId.getPassword());					
	}
	
	@Test
	public void testeIsCartaoDataExpiracaoExpiradaInvalida() {
				
		thrown.expect(TransactionNotApprovedException.class);
        thrown.expectMessage(messageSource.getMessage("erro.cartao.data-expiracao.mensagem", null, request.getLocale()));
		
		cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), crtSemId.getNome(), "11/16", crtSemId.getCodigoSeguranca(), 
				                          tr.getValor(), crtSemId.getPassword());					
	}
	
	@Test
	public void testeIsCartaoNomeDiferenteInvalida() {
				
		thrown.expect(TransactionNotApprovedException.class);
        thrown.expectMessage(messageSource.getMessage("erro.cartao.nome-invalido.mensagem", null, request.getLocale()));
		
		cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), "Teste", crtSemId.getDataDeExpiracao(), crtSemId.getCodigoSeguranca(), 
				                          tr.getValor(), crtSemId.getPassword());					
	}

	@Test
	public void testeIsCartaoCodigoSegurancaoInvalida() {
				
		thrown.expect(TransactionNotApprovedException.class);
        thrown.expectMessage(messageSource.getMessage("erro.cartao.codigo-segurancao.mensagem", null, request.getLocale()));
		
		cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), crtSemId.getNome(), crtSemId.getDataDeExpiracao(), "222", 
				tr.getValor(), crtSemId.getPassword());					
	}

	@Test
	public void testeIsCartaoSaldoInsuficienteInvalida() {
				
		thrown.expect(TransactionNotApprovedException.class);
        thrown.expectMessage(messageSource.getMessage("erro.cartao.valor.mensagem", null, request.getLocale()));
		
		cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), crtSemId.getNome(), crtSemId.getDataDeExpiracao(), crtSemId.getCodigoSeguranca(), 
				                          new BigDecimal(2000), crtSemId.getPassword());					
	}
	
	@Test
	public void testeAtualizaSaldoCartaoOk() {	
		CartaoSaldo saldo = cartaoSaldoService.atualizarSaldoCartao(tr);
		assertEquals(new BigDecimal(900), saldo.getSaldo());	
		
	}
	
}
