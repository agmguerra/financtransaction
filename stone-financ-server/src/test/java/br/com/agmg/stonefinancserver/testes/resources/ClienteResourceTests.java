package br.com.agmg.stonefinancserver.testes.resources;


import static br.com.agmg.stonefinancserver.utils.StringUtils.asJsonString;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Optional;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.Cliente;
import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoDocumentoEnum;
import br.com.agmg.stonefinancserver.resource.ClienteResource;
import br.com.agmg.stonefinancserver.service.ClienteService;

/**
 * 
 * Testes da classe ClienteResource
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ClienteResource.class)
public class ClienteResourceTests {
	
	@Autowired
    private MockMvc mvc;
 
    @MockBean
    private ClienteService service;
    
    Cliente cliente = null;
    Cliente clienteSemId = null;
    
	@Before
	public void preparaTestes() {
		
		Cartao crt = new Cartao();
		crt.setId(new Long(1));
		crt.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crt.setCodigoSeguranca("111");
		crt.setDataDeExpiracao("12/18");
		crt.setNome("João da Silva");
		crt.setNumero("1234567890123456");
		crt.setPossuiPassword(true);
		crt.setTipoCartao(TipoCartaoEnum.CHIP);
		
		clienteSemId =  new Cliente(null, "João da Silva", TipoDocumentoEnum.CPF, "24748634510");
		cliente = new Cliente(new Long(1), "João da Silva", TipoDocumentoEnum.CPF, "24748634510");
		cliente.setCartoes(Arrays.asList(crt));
    	
    	given(service.recuperarTodosClientes()).willReturn(Arrays.asList(cliente));
    	given(service.recuperarCliente(new Long(1))).willReturn(Optional.of(cliente));
    	given(service.gravarCliente(clienteSemId)).willReturn(cliente);

	}

    
    @Test
    public void recuperarClientesTeste() throws Exception {
    	    	    	
    	mvc.perform(get("/api/clientes")
    		      .contentType(MediaType.APPLICATION_JSON))
    		      .andExpect(status().isOk())
    		      .andExpect(jsonPath("$['_embedded']['clientes']", hasSize(1)))
    		      .andExpect(jsonPath("$['_embedded']['clientes'][0].nome", Is.is(cliente.getNome())));
    	
    }
 
    @Test
    public void recuperarClientePorIdTeste() throws Exception {
    	    	
    	mvc.perform(get("/api/clientes/1")
  		      .contentType(MediaType.APPLICATION_JSON))
  		      .andExpect(status().isOk())
  		      .andExpect(jsonPath("$.['nome']", Is.is(cliente.getNome())));

    	
    }
    
    @Test
    public void recuperarTodosCartoesByClienteId() throws Exception {
    	
       	mvc.perform(get("/api/clientes/1/cartoes")
    		      .contentType(MediaType.APPLICATION_JSON))
    		      .andExpect(status().isOk())
    		      .andExpect(jsonPath("$['_embedded']['cartaos']", hasSize(1)))
    		      .andExpect(jsonPath("$['_embedded']['cartaos'][0]['nome']", Is.is(cliente.getNome())));
    	
    }
    
	@Test
	public void gravarCliente() throws Exception {

		
		mvc.perform(post("/api/clientes")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(clienteSemId)))
				.andExpect(status().isCreated());

	}

}
