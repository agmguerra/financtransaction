package br.com.agmg.stonefinancserver.testes.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.agmg.stonefinancserver.StoneFinancServerApplication;
import br.com.agmg.stonefinancserver.entity.Cliente;
import br.com.agmg.stonefinancserver.enumeration.TipoDocumentoEnum;
import br.com.agmg.stonefinancserver.repositorio.ClienteRepository;
import br.com.agmg.stonefinancserver.service.ClienteService;
import br.com.agmg.stonefinancserver.service.ClienteServiceImpl;

/**
 * Classe para teste de ClienteoServicos
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoneFinancServerApplication.class)
public class ClienteServiceTests {

	@TestConfiguration
    static class ClienteServiceImplTestContextConfiguration {
  
        @Bean
        public ClienteService clienteService() {
            return new ClienteServiceImpl();
        }
    }
 
    @Autowired
    private ClienteService clienteService;
 
    @MockBean
    private ClienteRepository clienteRepository;
 
    
    Cliente clienteSemId = null;
    Cliente cliente = null;
    List<Cliente> clientes = null;
			
	@Before
	public void preparaTestes() {
		
		clienteSemId = new Cliente(null, "João da Silva", TipoDocumentoEnum.CPF, "13662044501");
		
		cliente = new Cliente(1L, "João da Silva", TipoDocumentoEnum.CPF, "13662044501");
		
		clientes = Arrays.asList(cliente, new Cliente(2L, "Jorge da Silva", TipoDocumentoEnum.CPF, "13662044502"));
		
		Mockito.when(clienteRepository.findById(cliente.getId()))
	      .thenReturn(Optional.of(cliente));
		
		Mockito.when(clienteRepository.findByDocumento(clienteSemId.getDocumento()))
		  .thenReturn(Optional.of(cliente));
		
		Mockito.when(clienteRepository.save(clienteSemId)).thenReturn(cliente);
		
		Mockito.when(clienteRepository.findAll()).thenReturn(Arrays.asList(cliente));
		
	}
	
	@Test
	public void testeIncluiClienteOk() {
				
		Cliente clienteSalvo = clienteService.gravarCliente(clienteSemId);	
		assertNotNull(clienteSalvo);
		assertEquals(cliente.getId(), clienteSalvo.getId());
					
	}
	
	@Test
	public void testeRecuperarClientePorId() {
		
		Optional<Cliente> cliRet = clienteService.recuperarCliente(cliente.getId());
	
		assertNotNull(cliRet);
	}
	
	@Test
	public void testeRecuperarClientePorDocumento() {
		
		Optional<Cliente> cliRet = clienteService.recuperarClientePorDocumento(clienteSemId.getDocumento());
		
		assertNotNull(cliRet);
		
	}
	
	@Test
	public void testeRecuperarTodosClientes() {
		List<Cliente> clientes = clienteService.recuperarTodosClientes();
		
		assertNotNull(clientes);
		assertEquals(1, clientes.size());
	}
	

}
