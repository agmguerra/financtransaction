package br.com.agmg.stonefinancserver.testes.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.agmg.stonefinancserver.StoneFinancServerApplication;
import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancserver.repositorio.CartaoRepository;
import br.com.agmg.stonefinancserver.service.CartaoService;
import br.com.agmg.stonefinancserver.service.CartaoServiceImpl;

/**
 * Classe para teste de CartaoServicos
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoneFinancServerApplication.class)
public class CartaoServiceTests {

	@TestConfiguration
    static class CartaoServiceImplTestContextConfiguration {
  
        @Bean
        public CartaoService cartaoService() {
            return new CartaoServiceImpl();
        }
    }
 
    @Autowired
    private CartaoService cartaoService;
 
    @MockBean
    private CartaoRepository cartaoRepository;
 
    
    Cartao crtSemId = null;
    Cartao crt = null;
			
	@Before
	public void preparaTestes() {
		
		crtSemId = new Cartao();
		crtSemId.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crtSemId.setCodigoSeguranca("111");
		crtSemId.setDataDeExpiracao("12/18");
		crtSemId.setNome("Joao da Silva");
		crtSemId.setNumero("1234567890123456");
		crtSemId.setPassword("1234");
		crtSemId.setPossuiPassword(true);
		crtSemId.setTipoCartao(TipoCartaoEnum.CHIP);
			
		crt = new Cartao();
		crt.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crt.setCodigoSeguranca("111");
		crt.setDataDeExpiracao("12/18");
		crt.setNome("Joao da Silva");
		crt.setNumero("1234567890123456");
		crtSemId.setPassword("1234");
		crt.setPossuiPassword(true);
		crt.setTipoCartao(TipoCartaoEnum.CHIP);

				
		Mockito.when(cartaoRepository.findByNumero(crtSemId.getNumero()))
		  .thenReturn(Optional.of(crt));
		
		Mockito.when(cartaoRepository.save(crtSemId)).thenReturn(crt);

		Mockito.when(cartaoRepository.findById(crt.getId()))
	      .thenReturn(Optional.of(crt));
		
		
		Mockito.when(cartaoRepository.findAll()).thenReturn(Arrays.asList(crt));
	
		
	}
	
	@Test
	public void testeIncluiCartaoOk() {
				
		Cartao cartaoSalvo = cartaoService.gravarCartao(crtSemId);	
		assertNotNull(cartaoSalvo);
					
	}


	@Test
	public void testeRecuperarCartaoPorId() {
		
		Optional<Cartao> crtRet = cartaoService.recuperarCartao(crt.getId());
	
		assertNotNull(crtRet);
	}
	
	@Test
	public void testeRecuperarClientePorNumero() {
		
		Optional<Cartao> crtRet = cartaoService.recuperarCartaoPorNumero(crtSemId.getNumero());
		
		assertNotNull(crtRet);
		
	}
	
	@Test
	public void testeRecuperarTodosCartoes() {
		List<Cartao> cartoes = cartaoService.recuperarTodosCartoes();
		
		assertNotNull(cartoes);
		assertEquals(1, cartoes.size());
	}

}
