package br.com.agmg.stonefinancserver.testes.resources;


import static br.com.agmg.stonefinancserver.utils.StringUtils.asJsonString;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.Transacao;
import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoTransacaoEnum;
import br.com.agmg.stonefinancserver.resource.TransacaoResource;
import br.com.agmg.stonefinancserver.service.TransacaoService;

/**
 * 
 * Teste da classe TransacaoResource
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(TransacaoResource.class)
public class TransacaoResourceTests {
	
	@Autowired
    private MockMvc mvc;
 
    @MockBean
    private TransacaoService service;
    
    Transacao tr = null;
    Transacao trSemId = null;
    Cartao crt = null;
    Cartao crtSemId = null;
    
	@Before
	public void preparaTestes() {

		Cartao crt = new Cartao();
		crt.setId(new Long(1));
		crt.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crt.setCodigoSeguranca("111");
		crt.setDataDeExpiracao("12/18");
		crt.setNome("João da Silva");
		crt.setNumero("1234567890123456");
		crt.setPossuiPassword(true);
		crt.setTipoCartao(TipoCartaoEnum.CHIP);

		tr = new Transacao();
		tr.setId(new Long(1));
		tr.setDataTransacao(new Date());
		tr.setNumeroParcelas(0);
		tr.setTipoTransacao(TipoTransacaoEnum.CREDITO);
		tr.setValor(new BigDecimal(100));
		tr.setCartao(crt);
    	
		trSemId = new Transacao();
		BeanUtils.copyProperties(tr, trSemId);
		trSemId.setId(null);
		trSemId.getCartao().setId(null);
		
		given(service.recuperarTransacoes()).willReturn(Arrays.asList(tr));
    	given(service.recuperarTransacao(new Long(1))).willReturn(Optional.of(tr));
    	given(service.processarTransacao(trSemId)).willReturn(tr);

	}

    
    @Test
    public void recuperarTodasTransacoesTeste() throws Exception {
    	    	    	
    	mvc.perform(get("/api/transacoes")
    		      .contentType(MediaType.APPLICATION_JSON))
    		      .andExpect(status().isOk())
    		      .andExpect(jsonPath("$['_embedded']['transacaos']", hasSize(1)))
    		      .andExpect(jsonPath("$['_embedded']['transacaos'][0]['id']", Is.is(tr.getId().intValue())));
    	
    }
 
    @Test
    public void recuperarTransacaoPorIdTeste() throws Exception {
    	    	
    	mvc.perform(get("/api/transacoes/1")
  		      .contentType(MediaType.APPLICATION_JSON))
  		      .andExpect(status().isOk())
  		      .andExpect(jsonPath("$.['id']", Is.is(tr.getId().intValue())));

    	
    }
        
	@Test
	public void processarTransacaoTeste() throws Exception {

		
		mvc.perform(post("/api/transacoes")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(trSemId)))
				.andExpect(status().isCreated());

	}

}
