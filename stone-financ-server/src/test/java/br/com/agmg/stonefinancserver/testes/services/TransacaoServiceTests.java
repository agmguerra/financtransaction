package br.com.agmg.stonefinancserver.testes.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.agmg.stonefinancserver.StoneFinancServerApplication;
import br.com.agmg.stonefinancserver.entity.Cartao;
import br.com.agmg.stonefinancserver.entity.CartaoSaldo;
import br.com.agmg.stonefinancserver.entity.Transacao;
import br.com.agmg.stonefinancserver.enumeration.BandeiraCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoCartaoEnum;
import br.com.agmg.stonefinancserver.enumeration.TipoTransacaoEnum;
import br.com.agmg.stonefinancserver.repositorio.TransacaoRepository;
import br.com.agmg.stonefinancserver.service.CartaoSaldoService;
import br.com.agmg.stonefinancserver.service.CartaoService;
import br.com.agmg.stonefinancserver.service.TransacaoService;
import br.com.agmg.stonefinancserver.service.TransacaoServiceImpl;

/**
 * Classe para teste de TransacaoService
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoneFinancServerApplication.class)
public class TransacaoServiceTests {

	@TestConfiguration
    static class TransacaoServiceImplTestContextConfiguration {
  
        @Bean
        public TransacaoService TransacaoService() {
            return new TransacaoServiceImpl();
        }
    }
 
    @Autowired
    private TransacaoService transacaoService;
 
    @MockBean
    private CartaoSaldoService cartaoSaldoService;
    
    @MockBean
    private CartaoService cartaoService;    
    
    @MockBean
    private TransacaoRepository transacaoRepository;
 
    
    Transacao trSemId = null;
    Transacao tr = null;
    

			
	@Before
	public void preparaTestes() {
		
		CartaoSaldo crtSaldo = new CartaoSaldo();
		crtSaldo.setId(1L);
		crtSaldo.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crtSaldo.setCodigoSeguranca("111");
		crtSaldo.setDataDeExpiracao("12/18");
		crtSaldo.setNome("João da Silva");
		crtSaldo.setNumero("1234567890123456");
		crtSaldo.setPassword("1234");
		crtSaldo.setPossuiPassword(true);
		crtSaldo.setTipoCartao(TipoCartaoEnum.CHIP);

		
		Cartao crtSemId = new Cartao();
		crtSemId.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crtSemId.setCodigoSeguranca("111");
		crtSemId.setDataDeExpiracao("12/18");
		crtSemId.setNome("João da Silva");
		crtSemId.setNumero("1234567890123456");
		crtSemId.setPassword("1234");
		crtSemId.setPossuiPassword(true);
		crtSemId.setTipoCartao(TipoCartaoEnum.CHIP);
		
		Cartao crt = new Cartao();
		crt.setId(1L);
		crt.setBandeiraDoCartao(BandeiraCartaoEnum.MASTERCARD);
		crt.setCodigoSeguranca("111");
		crt.setDataDeExpiracao("12/18");
		crt.setNome("João da Silva");
		crt.setNumero("1234567890123456");
		crt.setPassword("1234");
		crt.setPossuiPassword(true);
		crt.setTipoCartao(TipoCartaoEnum.CHIP);


		trSemId = new Transacao();
		trSemId.setDataTransacao(new Date());
		trSemId.setNumeroParcelas(0);
		trSemId.setTipoTransacao(TipoTransacaoEnum.CREDITO);
		trSemId.setValor(new BigDecimal(100));
		trSemId.setCartao(crtSemId);

		tr = new Transacao();
		tr.setId(1L);
		tr.setDataTransacao(new Date());
		tr.setNumeroParcelas(0);
		tr.setTipoTransacao(TipoTransacaoEnum.CREDITO);
		tr.setValor(new BigDecimal(100));
		tr.setCartao(crtSemId);

		
		
		Mockito.when(transacaoRepository.findById(1L))
	      .thenReturn(Optional.of(tr));
		
		
		Mockito.when(transacaoRepository.save(trSemId)).thenReturn(tr);
		
		Mockito.when(transacaoRepository.findAll()).thenReturn(Arrays.asList(tr));
		
		Mockito.when(cartaoSaldoService.isCartaoValido(crtSemId.getNumero(), crtSemId.getNome(), crtSemId.getDataDeExpiracao(),
				crtSemId.getCodigoSeguranca(), trSemId.getValor(), crtSemId.getPassword())).thenReturn(true);
		
		Mockito.when(cartaoSaldoService.atualizarSaldoCartao(trSemId)).thenReturn(crtSaldo);
		
		Mockito.when(cartaoService.recuperarCartaoPorNumero(crtSemId.getNumero())).thenReturn(Optional.of(crt));
		

	}
	
	@Test
	public void testeProcessarTransacaoOk() {
				
		Transacao trProcessada = transacaoService.processarTransacao(trSemId);
		assertNotNull(trProcessada);
		assertEquals(tr.getId(), trProcessada.getId());
					
	}
	
	@Test
	public void testeRecuperarTransacaoPorId() {
		
		Optional<Transacao> trRet = transacaoService.recuperarTransacao(1L);
	
		assertNotNull(trRet);
	}
	
	
	@Test
	public void testeRecuperarTodasTransacoes() {
		List<Transacao> trs = transacaoService.recuperarTransacoes();
		
		assertNotNull(trs);
		assertEquals(1, trs.size());
	}
	

}
